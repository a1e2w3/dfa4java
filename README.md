An implementation of Deterministic Finite Automaton(DFA) in java. 

It contains the transform between regular expression and minimized DFA. Firstly, resolve the regular expression 
and build a NFA, than determinize the NFA, minimize the DFA we get.

It offers the intersect, complement, minus and equal operation in version 1.0, and union operation was added 
in v2.0. It can be used to calculate the intersection, union, complement and difference set of two regular 
set formulized by a regular expression. It also can judge whether two regular expressions were equal or not, 
judge whether the regular set was infinity set, and calculate the number of strings that the regular set 
contains and list all of them if it's a finitude set.

Developers can use class DFABuilder to get an instance of DFA. In addition to build a DFA from a regular 
expression, we offer two other ways: build a DFA accept only one string from a char sequence, or build a DFA
that accept strings between two boundary strings.