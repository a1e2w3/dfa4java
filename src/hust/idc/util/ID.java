package hust.idc.util;

import java.io.Serializable;
import java.util.UUID;

public abstract class ID implements Serializable, Comparable<ID> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2584066378164565393L;
	private UUID id;
	
	public ID(){
		this.id = UUID.randomUUID();
	}
	
	public ID(ID id){
		this.id = id.id;
	}

	@Override
	public int hashCode() {
		return (id == null) ? 0 : id.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (this.getClass() != obj.getClass())
			return false;
		ID other = (ID) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return id.toString();
	}

	@Override
	public int compareTo(ID o) {
		// TODO Auto-generated method stub
		if(o == null)
			return -1;
		if(o.id == null)
			return this.id == null ? 0 : -1;
		return this.id.compareTo(o.id);
	}

}
