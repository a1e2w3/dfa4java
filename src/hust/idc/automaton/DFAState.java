package hust.idc.automaton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * DFA状�?的数据结�?
 * 
 * @author WangCong
 * 
 */
class DFAState {
	private StateID stateId;
	private Map<CharRange, StateID> trasitionMap;

	/**
	 * Ĭ��ʹ�õĿ�ʼ״̬ID
	 */
	// protected static final StateID DEFAULT_START_STATE_ID = 0;
	/**
	 * ����Ľ���״̬ID
	 */
	// protected static final StateID DEFAULT_END_STATE_ID = -1;
	/**
	 * ʧ��״̬ID
	 */
	protected static final StateID FAILED_STATE_ID = new StateID();

	/**
	 * 默认终止状�?常量，一旦进入该状�?，无论再接受�?��字符都返回该状�?
	 */
	// protected static final DFAState END_STATE;
	/**
	 * 失败状�?常量，一旦进入该状�?，无论再接受�?��字符都返回该状�?
	 */
	protected static final DFAState FAILED_STATE;

	static {
		// END_STATE = new DFAState(DEFAULT_END_STATE_ID){
		// public void dispose(){
		//
		// }
		// protected final StateID getTransition(char ch){
		// return DEFAULT_END_STATE_ID;
		// }
		// protected final boolean isFailedStatus(){
		// return false;
		// }
		// protected final void addTransition(CharRange range, StateID transId){
		// throw new UnsupportedOperationException("Unsupported Operation");
		// }
		// protected final void addTransition(char ch, StateID transId){
		// throw new UnsupportedOperationException("Unsupported Operation");
		// }
		// protected final Set<Character> getStartPoints(){
		// Set<Character> pointset = new HashSet<Character>();
		// pointset.add(CharRange.CHAR_MIN_VALUE);
		// return pointset;
		// }
		// protected final List<CharRange> getFailedCharRangeList(){
		// return new ArrayList<CharRange>();
		// }
		// };
		// END_STATE.trasitionMap.put(CharRange.UNIVERSAL_RANGE,
		// DEFAULT_END_STATE_ID);

		FAILED_STATE = new DFAState(FAILED_STATE_ID) {
			public void dispose() {

			}

			protected final StateID getTransition(char ch) {
				return FAILED_STATE_ID;
			}

			protected final boolean isFailedStatus() {
				return true;
			}

			protected final void addTransition(CharRange range, StateID transId) {
				throw new UnsupportedOperationException("Unsupported Operation");
			}

			protected final void addTransition(char ch, StateID transId) {
				throw new UnsupportedOperationException("Unsupported Operation");
			}

			protected final Set<Character> getStartPoints() {
				Set<Character> pointset = new HashSet<Character>();
				pointset.add(CharRange.CHAR_MIN_VALUE);
				return pointset;
			}

			protected final List<CharRange> getFailedCharRangeList() {
				List<CharRange> list = new ArrayList<CharRange>();
				list.add(CharRange.UNIVERSAL_RANGE);
				return list;
			}
		};
	}

	protected DFAState() {
		this.stateId = new StateID();
		this.trasitionMap = new HashMap<CharRange, StateID>();
	}

	/**
	 * ����һ��DFA״̬��ǿ�ҽ���statusIdȡ�Ǹ�ֵ����ֵ����������״̬ID����
	 * 
	 * @param stateId
	 *            状�?ID
	 */
	protected DFAState(StateID stateId) {
		this.stateId = stateId;
		this.trasitionMap = new HashMap<CharRange, StateID>();
	}

	protected static DFAState defaultEndState() {
		DFAState endState = new DFAState();
		endState.addTransition(CharRange.UNIVERSAL_RANGE, endState.getStateId());
		return endState;
	}

	protected void dispose() {
		this.trasitionMap.clear();
		this.trasitionMap = null;
	}

	/**
	 * 获得状�?ID
	 * 
	 * @return 状�?ID
	 */
	protected final StateID getStateId() {
		return stateId;
	}

	/**
	 * 获得转换函数集合的深拷贝
	 * 
	 * @return 转换函数集合的深拷贝
	 */
	protected final Map<CharRange, StateID> getTransitionMap() {
		// Map<CharRange, Integer> map = new HashMap<CharRange, Integer>();
		// map.putAll(this.trasitionMap);
		// return map;
		return this.trasitionMap;
	}

	protected void combineTransitions() {
		DFAState tempState = new DFAState(this.stateId);
		Iterator<Entry<CharRange, StateID>> transIt = this
				.getTransitionIterator();
		while (transIt.hasNext()) {
			Entry<CharRange, StateID> entry = transIt.next();
			tempState.addTransition(entry.getKey(), entry.getValue());
			transIt.remove();
		}
		this.trasitionMap.putAll(tempState.getTransitionMap());
		tempState.dispose();
	}

	protected void addTransition(final CharRange charRange, DFAState state) {
		this.addTransition(charRange, state.getStateId());
	}

	protected void addTransition(char ch, DFAState state) {
		this.addTransition(ch, state.getStateId());
	}

	protected void addTransition(final CharRange charRange, StateID stateId) {
		List<CharRange> list = charRange.intersectWith(this
				.getFailedCharRangeList());
		for (CharRange cr : list)
			this.addTransitionWithoutCheck(cr, stateId);
	}

	protected void addTransition(char ch, StateID stateId) {
		StateID transId = this.getTransition(ch);
		if (transId == stateId)
			return;
		if (transId == FAILED_STATE_ID)
			this.addTransitionWithoutCheck(new CharRange(ch), stateId);
	}

	/**
	 * 加入�?��转换状�?
	 * 
	 * @param charRange
	 *            转换条件字符区间
	 * @param stateId
	 *            目标状�?的ID
	 * @throws Throwable
	 * @throws IllegalArgumentException
	 */
	private void addTransitionWithoutCheck(final CharRange charRange,
			StateID stateId) {
		char start = charRange.getStart();
		char end = charRange.getEnd();
		if (!charRange.isInRange()) {
			if (start > CharRange.CHAR_MIN_VALUE)
				this.addTransition(new CharRange(CharRange.CHAR_MIN_VALUE,
						(char) (start - 1)), stateId);
			if (end < CharRange.CHAR_MAX_VALUE)
				this.addTransition(new CharRange((char) (end + 1),
						CharRange.CHAR_MAX_VALUE), stateId);
			return;
		}

		if (start > CharRange.CHAR_MIN_VALUE
				&& this.getTransition((char) (start - 1)) == stateId) {
			for (Iterator<Entry<CharRange, StateID>> transIt = this
					.getTransitionIterator(); transIt.hasNext();) {
				Entry<CharRange, StateID> entry = transIt.next();
				if (entry.getKey().getEnd() + 1 == start) {
					transIt.remove();
					start = entry.getKey().getStart();
					if (end < CharRange.CHAR_MAX_VALUE
							&& this.getTransition((char) (end + 1)) == stateId) {
						for (Iterator<Entry<CharRange, StateID>> transIt2 = this
								.getTransitionIterator(); transIt2.hasNext();) {
							Entry<CharRange, StateID> entry2 = transIt2.next();
							if (entry2.getKey().getStart() == end + 1) {
								transIt2.remove();
								end = entry2.getKey().getEnd();
								break;
							}
						}
					}

					CharRange newRange = new CharRange(start, end);
					this.trasitionMap.put(newRange, stateId);
					return;
				}
			}
		} else if (end < CharRange.CHAR_MAX_VALUE
				&& this.getTransition((char) (end + 1)) == stateId) {
			for (Iterator<Entry<CharRange, StateID>> transIt = this
					.getTransitionIterator(); transIt.hasNext();) {
				Entry<CharRange, StateID> entry = transIt.next();
				if (entry.getKey().getStart() == end + 1) {
					transIt.remove();
					end = entry.getKey().getEnd();

					CharRange newRange = new CharRange(start, end);
					this.trasitionMap.put(newRange, stateId);
					return;
				}
			}
		}
		this.trasitionMap.put(charRange, stateId);
	}

	/**
	 * 获得�?��字符的转换目标状�?
	 * 
	 * @param ch
	 *            条件字符
	 * @return 转换到的目标状�?，没有合适的转换规则则认为转换到失败状�?，返回常量FAILED_STATUS_ID
	 */
	protected StateID getTransition(char ch) {
		if (this.trasitionMap.isEmpty())
			return FAILED_STATE_ID;
		for (Iterator<Entry<CharRange, StateID>> it = this
				.getTransitionIterator(); it.hasNext();) {
			Entry<CharRange, StateID> entry = it.next();
			if (entry.getKey().charIn(ch))
				return entry.getValue();
		}
		return FAILED_STATE_ID;
	}

	/**
	 * 获得�?��字符区间的转换目标状�?
	 * 
	 * @param range
	 *            字符区间
	 * @return 如果转换规则集合中存在一条转换规则的条件与range等价，则返回其目标状态ID，否则返回null
	 */
	protected final StateID getTransition(CharRange range) {
		if (this.trasitionMap.isEmpty())
			return FAILED_STATE_ID;
		return this.trasitionMap.get(range);
	}

	/**
	 * 获得转换规则集合的迭代器，用于迭代和修改转换规则
	 * 
	 * @return 转换规则集合的迭代器
	 */
	protected final Iterator<Entry<CharRange, StateID>> getTransitionIterator() {
		return this.trasitionMap.entrySet().iterator();
	}

	/**
	 * 获得该状态所有转换到失败状�?的字符集合，以一列字符区间的并集的形式表�?
	 * 
	 * @return 该状态所有转换到失败状�?的字符集合，以一列字符区间的并集的形式表�?
	 * @throws Throwable
	 */
	protected List<CharRange> getFailedCharRangeList() {
		List<CharRange> list = CharRangeOperation
				.getComplement(this.trasitionMap.keySet());

		if (this.trasitionMap.containsValue(FAILED_STATE_ID)) {
			Iterator<Entry<CharRange, StateID>> it = this
					.getTransitionIterator();
			while (it.hasNext()) {
				Entry<CharRange, StateID> entry = it.next();
				if (entry.getValue().equals(FAILED_STATE_ID))
					list.add(entry.getKey());
			}
		}

		return list;
	}

	/**
	 * 该状态所有转换规则的条件产生�?��对字母表的划分，获得该划分的�?��字符区间的起始点
	 * 可用来代替字母表，起始点的行为可代替其所在区间的全体字符的行�? 结果以字符集合表示，没有排序
	 * 
	 * @return �?��起始点的集合
	 */
	protected Set<Character> getStartPoints() {
		Set<Character> pointset = new HashSet<Character>();
		pointset.add(CharRange.CHAR_MIN_VALUE);
		for (CharRange cr : this.trasitionMap.keySet()) {
			pointset.add(cr.getStart());
			if (cr.getEnd() < CharRange.CHAR_MAX_VALUE)
				pointset.add((char) (cr.getEnd() + 1));
		}
		return pointset;
	}

	/**
	 * 判断该状态是否是失败状�?
	 * 
	 * @return 是终止状态返回true，否则返回false
	 */
	protected boolean isFailedStatus() {
		return this.getStateId() == FAILED_STATE_ID;
	}

	/**
	 * 控制台打印转换函数集合，调试�?
	 */
	protected final void printTransition() {
		if (this.trasitionMap.isEmpty())
			return;
		else
			System.out.print(this.transitionToString());
	}

	private StringBuilder transitionToString() {
		if (this.trasitionMap.isEmpty())
			return new StringBuilder();
		StringBuilder builder = new StringBuilder();
		for (Iterator<Entry<CharRange, StateID>> it = this
				.getTransitionIterator(); it.hasNext();) {
			Entry<CharRange, StateID> entry = it.next();
			StateID transId = entry.getValue();
			String print = this.stateId + " + char in "
					+ entry.getKey().toString() + " to " + transId;
			if (transId.equals(FAILED_STATE_ID))
				print += " Failed!";
			builder.append(print);
			builder.append("\n");
		}
		return builder;
	}

	/**
	 * ��дtoString��������ʾ״̬ID��״̬ת������
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder("State Id = " + this.stateId);
		// if(this.stateId == DEFAULT_START_STATUS_ID)
		// builder.append(" Start! ");
		builder.append("\n");
		builder.append(this.transitionToString());
		return builder.toString();
	}
}