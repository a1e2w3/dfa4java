package hust.idc.automaton;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * DFA建�?�?
 * @author WangCong
 *
 */
public final class DFABuilder {
	private DFAState startState;
	private Map<StateID, DFAState> statesMap;
	private Set<StateID> endStateIds;
	
	/**
	 * 实例化DFA�?��建�?�?
	 */
	public DFABuilder(){}
	
	private final void init(){
		this.startState = new DFAState();		
		this.statesMap = new HashMap<StateID, DFAState>();
		this.putState(this.startState);
		
		this.endStateIds = new HashSet<StateID>();
	}
	
	private final void cleanup(){
		// cannot clear the map and set
		this.startState = null;
		this.statesMap = null;
		this.endStateIds = null;
	}
	
	/**
	 * 通过正则式获得一个DFA对象
	 * @param pattern 正则�?
	 * @return 正则式等价的DFA
	 * @throws Throwable 
	 */
	public DFA getInstance(Pattern pattern) {
		NFABuilder builder = new NFABuilder();
		NFA nfa = builder.getInstance(pattern);
		DFA dfa = nfa.determinize();
		nfa.dispose();
		dfa.setPattern(pattern);
		return dfa;
	}
	
	/**
	 * 根据字符串区间获得有限自动机对象
	 * @param lower 字符串区间下限，null表示无下�?
	 * @param lowerEqual 下限是否为闭区间
	 * @param upper 字符串区间上限，null表示无上�?
	 * @param upperEqual 上限是否为闭区间
	 * @return 构�?的有限自动机对象
	 * @throws Throwable 
	 */
	public DFA getInstance(String lower, boolean lowerEqual, String upper, boolean upperEqual) {
		if(lower == null || lower.length() <= 0){
			return this.createFiniteAutomatonByLessRange(upper, upperEqual);
		} else if(upper == null || upper.length() <= 0) {
			return this.createFiniteAutomatonByGreaterRange(lower, lowerEqual);
		} else {
			int comp = lower.compareTo(upper);
			if(comp > 0)
				return DFA.EMPTY_DFA;
			else if(comp == 0){
				if(!lowerEqual || !upperEqual)
					return DFA.EMPTY_DFA;
				else
					return this.getInstance(lower, true);
			} else {
				DFA faless = this.createFiniteAutomatonByLessRange(upper, upperEqual);
				DFA fagreater = this.createFiniteAutomatonByGreaterRange(lower, lowerEqual);
				return faless.intersectWith(fagreater);
			}
		}
	}
	
	/**
	 * 构�?只接受一个字符序列的DFA
	 * @param sequence 字符序列
	 * @return 只接受序列sequence的DFA
	 * @throws Throwable 
	 */
	public DFA getInstance(CharSequence sequence, boolean caseIntensive) {
		return this.createFiniteAutomatonByStringEqual(sequence, caseIntensive);
	}
	
	public DFA getInstance(CharSequence sequence) {
		return this.createFiniteAutomatonByStringEqual(sequence, true);
	}
	
	private DFA createFiniteAutomatonByStringEqual(CharSequence sequence, boolean caseIntensive) {
		this.init();
		if(sequence == null || sequence.length() <= 0)
			return DFA.EMPTY_DFA;
		
		DFAState now = this.startState;
		int length = sequence.length();
		for(int i = 0; i < length - 1; ++i){
			char ch = sequence.charAt(i);
			DFAState newStatus = this.generateState();
			
			if(!caseIntensive && Character.isLetter(ch)){
				now.addTransition(Character.toLowerCase(ch), newStatus.getStateId());
				now.addTransition(Character.toUpperCase(ch), newStatus.getStateId());
			} else 
				now.addTransition(ch, newStatus.getStateId());
			
			this.putState(newStatus);
			now = newStatus;
		}
		
		char ch = sequence.charAt(length - 1);
		DFAState newStatus = this.generateState();
		now.addTransition(ch, newStatus.getStateId());
		this.putState(newStatus);
		this.endStateIds.add(newStatus.getStateId());
		DFA dfa = new DFA(this.startState, this.statesMap, this.endStateIds, true);
		dfa.setPattern(Pattern.compile(CharacterEscaper.getEscape(sequence, caseIntensive).toString()));
		return dfa;
	}
	
	private DFA createFiniteAutomatonByLessRange(String bound, boolean boundEqual) {
		// TODO Auto-generated method stub
		this.init();
		if(bound == null || bound.length() <= 0)
			return DFA.FULL_DFA;
		
		DFAState defaultEndState = DFAState.defaultEndState();
		this.putState(defaultEndState, true);
		
		this.endStateIds.add(this.startState.getStateId());
		DFAState now = this.startState;

		int length = bound.length();
		for(int i = 0; i < length - 1; ++i){
			char low = bound.charAt(i);
			if(low > CharRange.CHAR_MIN_VALUE){
				CharRange cr = new CharRange(CharRange.CHAR_MIN_VALUE, (char) (low - 1));
				now.addTransition(cr, defaultEndState.getStateId());
			}
			
			CharRange lowEqual = new CharRange(low);
			DFAState newState = this.generateState();
			now.addTransition(lowEqual, newState.getStateId());
			this.putState(newState, true);
			now = newState;
		}
	
		char low = bound.charAt(length - 1);
		if(low > CharRange.CHAR_MIN_VALUE){
			CharRange cr = new CharRange(CharRange.CHAR_MIN_VALUE, (char) (low - 1));
			now.addTransition(cr, defaultEndState.getStateId());
		}
		
		if(boundEqual){
			CharRange lowEqual = new CharRange(low);
			DFAState endState = this.generateState();
			now.addTransition(lowEqual, endState.getStateId());
			
			this.putState(endState, true);
		} 
		
		DFA dfa = new DFA(this.startState, this.statesMap, this.endStateIds);
		dfa.minimize();
		this.cleanup();
		return dfa;
	}
	
	private DFA createFiniteAutomatonByGreaterRange(String bound,
			boolean boundEqual) {
		// TODO Auto-generated method stub
		this.init();
		if(bound == null || bound.length() <= 0)
			return DFA.FULL_DFA;

		DFAState defaultEndState = DFAState.defaultEndState();
		this.putState(defaultEndState, true);
		
		DFAState now = this.startState;
		int length = bound.length();
		for(int i = 0; i < length - 1; ++i){
			char up = bound.charAt(i);
			
			if(up < CharRange.CHAR_MAX_VALUE){
				CharRange cr = new CharRange((char) (up + 1), CharRange.CHAR_MAX_VALUE);
				now.addTransition(cr, defaultEndState.getStateId());
			}
			
			CharRange upEqual = new CharRange(up);
			DFAState newState = this.generateState();
			now.addTransition(upEqual, newState.getStateId());
			this.putState(newState);
//			now.printTransition();
//			newStatus.printTransition();
			now = newState;
		}
		
		char up = bound.charAt(length - 1);
		if(up < CharRange.CHAR_MAX_VALUE){
			CharRange cr = new CharRange((char) (up + 1), CharRange.CHAR_MAX_VALUE);
			now.addTransition(cr, defaultEndState.getStateId());
		}
		
		CharRange upEqual = new CharRange(up);
		DFAState newState = this.generateState();
		newState.addTransition(CharRange.UNIVERSAL_RANGE, defaultEndState.getStateId());
		this.putState(newState, boundEqual);
		now.addTransition(upEqual, newState.getStateId());
		
		DFA dfa = new DFA(this.startState, this.statesMap, this.endStateIds);
		dfa.minimize();
		this.cleanup();
		return dfa;
	}
	
	private final DFAState generateState(){
		return new DFAState();
	}
	
	private final void putState(DFAState state){
		this.putState(state, false);
	}
	
	private final void putState(DFAState state, boolean isEndState){
		this.statesMap.put(state.getStateId(), state);
		if(isEndState){
			this.endStateIds.add(state.getStateId());
		}
	}
}
