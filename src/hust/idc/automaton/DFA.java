package hust.idc.automaton;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * DFA数据结构
 * 
 * @author WangCong
 * 
 */
public class DFA implements Cloneable {
	private transient Pattern pattern;
	private long acceptStringCount = UNCHECKED;
	private Map<StateID, DFAState> statesMap;
	private Set<StateID> endStateIds;
	private final StateID startStateId;
	private boolean minimized = false;

	private static final long INFINITY = Long.MAX_VALUE;
	private static final long UNCHECKED = -1L;
	public static final DFA EMPTY_DFA;
	public static final DFA FULL_DFA;

	static {
		EMPTY_DFA = new DFA() {
			@Override
			protected Map<StateID, DFAState> getStatesMap(){
				throw new UnsupportedOperationException("Unsupported operation: getStatesMap()");
			}
			
			@Override
			protected Set<StateID> getEndStateIds(){
				return new HashSet<StateID>();
			}

			@Override
			public final void dispose() {

			}

			@Override
			public final boolean isEmpty() {
				return true;
			}

			@Override
			public final boolean match(CharSequence input) {
				return false;
			}

			@Override
			public final boolean matchNullSeq() {
				return false;
			}

			@Override
			public final void setPattern(Pattern pattern) {
			}

			@Override
			public final void clearPattern() {
			}

			@Override
			public final long acceptStringCount() {
				return 0L;
			}

			@Override
			public final boolean canAcceptFiniteString() {
				return false;
			}

			@Override
			public final String getExample(int length) {
				return null;
			}

			@Override
			public final int minDepth() {
				return -1;
			}

			@Override
			public final int maxDepth() {
				return 0;
			}
		};

		FULL_DFA = new DFA() {
			@Override
			protected Map<StateID, DFAState> getStatesMap(){
				throw new UnsupportedOperationException("Unsupported operation: getStatesMap()");
			}

			@Override
			protected Set<StateID> getEndStateIds(){
				Set<StateID> set = new HashSet<StateID>();
				set.add(this.getStartStateId());
				return set;
			}

			@Override
			public final void dispose() {

			}

			@Override
			public final boolean isEmpty() {
				return false;
			}

			@Override
			public final boolean match(CharSequence input) {
				return true;
			}

			@Override
			public final boolean matchNullSeq() {
				return true;
			}

			@Override
			public final void setPattern(Pattern pattern) {
			}

			@Override
			public final void clearPattern() {
			}

			@Override
			public final long acceptStringCount() {
				return INFINITY;
			}

			@Override
			public final boolean canAcceptFiniteString() {
				return true;
			}

			@Override
			public final String getExample(int length) {
				if (length < 0)
					return "automaton";
				StringBuilder builder = new StringBuilder(length);
				for (int i = 0; i < length; ++i)
					builder.append('a');
				return builder.toString();
			}

			@Override
			public final int minDepth() {
				return 0;
			}

			@Override
			public final int maxDepth() {
				return Integer.MAX_VALUE;
			}
		};
		DFAState start = FULL_DFA.getStartState();
		start.addTransition(CharRange.UNIVERSAL_RANGE, start);
		FULL_DFA.endStateIds.add(start.getStateId());
		FULL_DFA.pattern = Pattern.compile("[\\d\\D]*");
		FULL_DFA.acceptStringCount = INFINITY;
	}

	private DFA() {
		this(new DFAState());
	}

	protected DFA(DFAState startState) {
		this.pattern = null;
		this.statesMap = new HashMap<StateID, DFAState>();
		this.endStateIds = new HashSet<StateID>();
		if(startState == null){
			DFAState start = new DFAState();
			this.startStateId = start.getStateId();
			this.statesMap.put(this.startStateId, start);
		} else {
			this.startStateId = startState.getStateId();
			this.statesMap.put(this.startStateId, startState);
		}
		this.acceptStringCount = 0L;
		this.minimized = true;
		this.pattern = Pattern.compile("[\\d&&\\D]");
	}
	
	protected DFA(DFAState startState, final Map<StateID, DFAState> statesMap,
			final Set<StateID> endStateIds) {
		this(startState.getStateId(), statesMap, endStateIds, false);
	}
	
	protected DFA(DFAState startState, final Map<StateID, DFAState> statesMap,
			final Set<StateID> endStateIds, boolean minimized) {
		this(startState.getStateId(), statesMap, endStateIds, minimized);
	}
	protected DFA(StateID startStateId, final Map<StateID, DFAState> statesMap,
			final Set<StateID> endStateIds) {
		this(startStateId, statesMap, endStateIds, false);
	}

	protected DFA(StateID startStateId, final Map<StateID, DFAState> statesMap,
			final Set<StateID> endStateIds, boolean minimized) {
		this.startStateId = startStateId;
		this.pattern = null;
//		this.acceptStringCount = UNCHECKED;
		// this.statusMap = copyStatusMap(statusMap);
		this.statesMap = statesMap == null ? new HashMap<StateID, DFAState>()
				: statesMap;
		this.endStateIds = new HashSet<StateID>();
		this.endStateIds.addAll(endStateIds);
		this.minimized = minimized;
	}

	public void dispose() {
		this.pattern = null;
		this.acceptStringCount = 0;
		Iterator<DFAState> iterator = this.getStateIterator();
		while (iterator.hasNext()) {
			iterator.next().dispose();
			iterator.remove();
		}
		this.statesMap = null;
		this.endStateIds.clear();
		this.endStateIds = null;
	}

	private final DFA copy() {
		StateID newStartStateId = null;
		Map<StateID, DFAState> newStatesMap = new HashMap<StateID, DFAState>();
		Set<StateID> newEndStateIds = new HashSet<StateID>();
		Map<StateID, StateID> statesIdMap = new HashMap<StateID, StateID>();
		
		Iterator<DFAState> statesIt = this.getStateIterator();
		while (statesIt.hasNext()) {
			DFAState state = statesIt.next();
			DFAState newState = new DFAState();
			newStatesMap.put(newState.getStateId(), newState);
			statesIdMap.put(state.getStateId(), newState.getStateId());
			
			if(this.isEndState(state.getStateId()))
				newEndStateIds.add(newState.getStateId());
			
			if(this.isStartState(state.getStateId()))
				newStartStateId = newState.getStateId();
		}
		
		statesIt = this.getStateIterator();
		while (statesIt.hasNext()) {
			DFAState state = statesIt.next();
			DFAState newState = newStatesMap.get(statesIdMap.get(state.getStateId()));
			Iterator<Entry<CharRange, StateID>> transIt = state.getTransitionIterator();
			while(transIt.hasNext()){
				Entry<CharRange, StateID> transEntry = transIt.next();
				newState.addTransition(transEntry.getKey(), statesIdMap.get(transEntry.getValue()));
			}
		}
		DFA dfa = new DFA(newStartStateId, newStatesMap, newEndStateIds, this.minimized);
		dfa.acceptStringCount = this.acceptStringCount;
		dfa.pattern = this.pattern;
		statesIdMap.clear();
		return dfa;
	}
	
	protected Map<StateID, DFAState> getStatesMap(){
		return this.statesMap;
	}
	
	protected Set<StateID> getEndStateIds(){
		return this.endStateIds;
	}

	protected final boolean containsState(StateID statusId) {
		return this.statesMap.containsKey(statusId);
	}

	protected final Iterator<DFAState> getStateIterator() {
		return new Iterator<DFAState>() {
			private Iterator<Entry<StateID, DFAState>> iterator = statesMap
					.entrySet().iterator();

			@Override
			public boolean hasNext() {
				// TODO Auto-generated method stub
				return iterator.hasNext();
			}

			@Override
			public DFAState next() {
				// TODO Auto-generated method stub
				return iterator.next().getValue();
			}

			@Override
			public void remove() {
				// TODO Auto-generated method stub
				iterator.remove();
			}

		};
	}
	
	protected final Iterator<DFAState> getEndStateIterator() {
		return new Iterator<DFAState>() {
			private Iterator<StateID> iterator = endStateIds.iterator();

			@Override
			public boolean hasNext() {
				// TODO Auto-generated method stub
				return iterator.hasNext();
			}

			@Override
			public DFAState next() {
				// TODO Auto-generated method stub
				StateID id = iterator.next();
				return getState(id);
			}

			@Override
			public void remove() {
				// TODO Auto-generated method stub
				iterator.remove();
			}

		};
	}

	protected final DFAState getState(StateID stateId) {
		if (stateId.equals(DFAState.FAILED_STATE_ID))
			return DFAState.FAILED_STATE;
		else
			return this.statesMap.get(stateId);
	}

	protected final StateID getStartStateId() {
		return this.startStateId;
	}

	protected final DFAState getStartState() {
		return this.getState(this.getStartStateId());
	}

	protected final Iterator<StateID> getEndStatesIterator() {
		return this.endStateIds.iterator();
	}

	protected void setPattern(Pattern pattern) {
		this.pattern = pattern;
	}

	/**
	 * 获得与DFA等价的正则式
	 * 
	 * @return 与DFA等价的正则式
	 */
	public final Pattern getPattern() {
		if (this.pattern == null && !this.isEmpty())
			generatePattern();
		return this.pattern;
	}

	protected final boolean isEndState(StateID stateId) {
		return this.endStateIds.contains(stateId);
	}
	
	protected final boolean isStartState(StateID stateId){
		return this.startStateId != null && this.startStateId.equals(stateId);
	}

	protected final int stateCount() {
		return this.statesMap.size();
	}

	protected final int endStateCount() {
		return this.endStateIds.size();
	}

	public void clearPattern() {
		this.pattern = null;
	}

	/**
	 * 判断该DFA内容是否为空，内容不为空不代表DFA一定不是空集，内容为空一定表示空集
	 * 
	 * @return DFA内容为空返回true，否则返回false
	 */
	public boolean isEmpty() {
		return this.statesMap == null || this.endStateIds == null
				|| this.statesMap.isEmpty() || this.endStateIds.isEmpty()
				|| !this.containsState(this.startStateId);
	}

	protected final char[] getStartPoints() {
		Set<Character> pointset = new HashSet<Character>();
		for (DFAState status : this.statesMap.values()) {
			pointset.addAll(status.getStartPoints());
		}

		char[] points = new char[pointset.size()];
		int count = 0;
		for (Character ch : pointset)
			points[count++] = ch;

		Arrays.sort(points);
		return points;
	}

	/**
	 * DFA表示字符串集合的基数
	 * 
	 * @return DFA接受的字符串的个数
	 */
	public long acceptStringCount() {
		if (this.acceptStringCount == UNCHECKED) {
			long count = DFAUtil.acceptStringCount(this);
			this.acceptStringCount = count < 0 ? INFINITY : count;
		}
		return this.acceptStringCount;
	}

	public boolean canAcceptFiniteString() {
		return this.acceptStringCount() == INFINITY;
	}

	public Set<String> acceptStrings() {
		return this.acceptStrings(-1);
	}

	public Set<String> acceptStrings(int maxSize) {
		return DFAUtil.acceptStrings(this, maxSize);
	}

	public String getExample() {
		return this.getExample(-1);
	}

	public String getExample(int length) {
		return DFAUtil.getExample(this, length);
	}

	public int minDepth() {
		return DFAUtil.minDepth(this);
	}

	public int maxDepth() {
		return DFAUtil.maxDepth(this);
	}

	/**
	 * 判断自动机表示的字符串集合是否包含一个字符序列
	 * 
	 * @param input
	 *            要检验的字符序列
	 * @return 包含该字符序列返回true，否则返回false
	 */
	public boolean match(CharSequence input) {
		if (this.isEmpty())
			return false;
		DFAState now = this.getStartState();
		if (now == null)
			return false;
		for (int i = 0; i < input.length(); ++i) {
			char ch = input.charAt(i);
			now = this.getTransition(now, ch);
			if (now == null || now.isFailedStatus())
				return false;
		}
		return this.isEndState(now.getStateId());
	}

	/**
	 * 该DFA是否匹配空字符串
	 * 
	 * @return 能匹配空串返回true，否则返回false
	 */
	public boolean matchNullSeq() {
		if (this.isEmpty())
			return false;
		try {
			return this.isEndState(this.startStateId)
					&& this.containsState(this.startStateId);
		} catch (Exception e) {
			return false;
		}
	}

	private final void generatePattern() {
		// TODO Auto-generated method stub
		if (this.isEmpty())
			return;
		PatternCreator creator = new PatternCreator(this);
		this.pattern = creator.createPattern();
	}

	private final DFAState getTransition(DFAState status, char ch) {
		if (status == null)
			return DFAState.FAILED_STATE;
		StateID transId = status.getTransition(ch);
		return this.getState(transId);
	}

	/**
	 * 求与该DFA表示字符串集合的补集等价的DFA
	 * 
	 * @return 与该DFA表示字符串集合的补集等价的DFA
	 */
	public final DFA complement() {
		if (this.isEmpty())
			return FULL_DFA;
		DFA complement = DFAUtil.getComplement(this);
		return complement.isEmpty() ? EMPTY_DFA : complement;
	}

	/**
	 * 该DFA与另一个DFA表示字符串集合的交集等价的DFA
	 * 
	 * @param anotherDFA
	 *            另一个DFA
	 * @return 与字符串集合交集等价的DFA
	 */
	public final DFA intersectWith(DFA anotherDFA) {
		if (anotherDFA == null || anotherDFA.isEmpty())
			return EMPTY_DFA;
		DFA intersection = DFAUtil.intersect(this, anotherDFA);
		return intersection.isEmpty() ? EMPTY_DFA : intersection;
	}
	
	/**
	 * 该DFA与另一个DFA表示字符串集合的并集等价的DFA
	 * 
	 * @param anotherDFA
	 *            另一个DFA
	 * @return 与字符串集合并集等价的DFA
	 */
	public final DFA unionWith(DFA anotherDFA){
		return DFAUtil.union(this, anotherDFA);
	}

	/**
	 * 求DFA与另一个DFA表示字符串集合的差集等价的DFA
	 * 
	 * @param subtractorDFA
	 *            被减的DFA
	 * @return 与字符串集合差集等价的DFA
	 */
	public final DFA minus(DFA subtractorDFA) {
		if (subtractorDFA == null || subtractorDFA.isEmpty())
			return this;
		DFA subtraction = DFAUtil.minus(this, subtractorDFA);
		return subtraction.isEmpty() ? EMPTY_DFA : subtraction;
	}

	/**
	 * DFA的最小化
	 * 
	 * @return 该DFA最小化后的DFA
	 */
	public final void minimize() {
		if(this.minimized() || this.isEmpty())
			return ;
		DFAMinimizer minimizer = new DFAMinimizer(this);
		minimizer.minimize();
		this.minimized = true;
		this.clearPattern();
	}
	
	public final boolean minimized(){
		return this.minimized;
	}
	
	public void resetCaches(){
		this.minimized = false;
		this.acceptStringCount = UNCHECKED;
		this.clearPattern();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equal() 两个有限自动机所表示的字符串集合等价时返回true
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof DFA))
			return false;
		return DFAUtil.equals(this, (DFA) obj);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		Iterator<DFAState> it = this.getStateIterator();
		while (it.hasNext()) {
			DFAState state = it.next();
			if(this.isStartState(state.getStateId())){
				builder.append("Start State: ");
			}
			builder.append(state.toString());
			builder.append("\n");
		}

		builder.append("End State: "); 
		builder.append(this.endStateIds.toString());
		builder.append("\n");
		return builder.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#clone()
	 */
	@Override
	public DFA clone() {
		// TODO Auto-generated method stub
		return this.copy();
	}

}
