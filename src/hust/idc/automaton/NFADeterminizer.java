package hust.idc.automaton;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Stack;

class NFADeterminizer {
	private NFA nfa;
	private Map<StateID, DFAState> statesMap;
	private Set<StateID> endStateIds;
	private DFAState startState;
	
	protected NFADeterminizer(NFA nfa){
		this.nfa = nfa;
	}
	
	private final void init(){
		this.startState = new DFAState();

		this.statesMap = new HashMap<StateID, DFAState>();
		this.putState(startState);

		this.endStateIds = new HashSet<StateID>();
	}
	
	private void cleanup(){
		// cannot clear the map and set
		this.startState = null;
		this.statesMap = null;
		this.endStateIds = null;
	}
	
	private final void putState(DFAState state){
		this.statesMap.put(state.getStateId(), state);
	}
	
	private final DFAState generateState(){
		return new DFAState();
	}
	
	protected DFA determinize() {
		if(nfa == null || nfa.isEmpty())
			return DFA.EMPTY_DFA;
		
		this.init();
		Map<StateID, DFAToNFAMap> dfaTonfaMap = new HashMap<StateID, DFAToNFAMap>();
		
		Stack<StateID> untagedStates = new Stack<StateID>();
		DFAToNFAMap dfaStart = new DFAToNFAMap(this.startState.getStateId());
		dfaTonfaMap.put(this.startState.getStateId(), dfaStart);
		Set<StateID> nullClosure = nfa.nullClosure(nfa.getStartStateId());
		dfaStart.addNFAState(nullClosure);
		for(StateID id : nullClosure){
			if(nfa.isEndState(id)){
				this.endStateIds.add(this.startState.getStateId());
				break;
			}
		}

		untagedStates.push(this.startState.getStateId());
		List<Character> alphabet = nfa.getStartPoints();
		while(!untagedStates.empty()){
			StateID currentId = untagedStates.pop();
			DFAState currentState = this.statesMap.get(currentId);
			DFAToNFAMap currentDFAMap = dfaTonfaMap.get(currentId);
			for(int i = 0; i < alphabet.size(); ++i){
				Set<StateID> nfaStatusSet = nfa.getTransition(currentDFAMap.nfaStatusSet, alphabet.get(i));
				if(nfaStatusSet.isEmpty())
					continue;
				Set<StateID> temp = nfa.nullClosure(nfaStatusSet);
				if(temp.isEmpty())
					continue;
				
				CharRange range = null;
				if(i < alphabet.size() - 1)
					range = new CharRange(alphabet.get(i), (char) (alphabet.get(i + 1) - 1));
				else
					range = new CharRange(alphabet.get(i), CharRange.CHAR_MAX_VALUE);
				boolean needNew = true;
				Iterator<StateID> stateIt = dfaTonfaMap.keySet().iterator();
				while(stateIt.hasNext()){
					StateID stateId = stateIt.next();
					DFAToNFAMap dfaMap = dfaTonfaMap.get(stateId);
					if(dfaMap.contains(temp)){
						currentState.addTransition(range, dfaMap.dfaStatusId);
						needNew = false;
						break;
					}
				}
				
				if(needNew) {
					DFAState newStatus = this.generateState();
					DFAToNFAMap dfaMap = new DFAToNFAMap(newStatus.getStateId());
					dfaMap.addNFAState(temp);
					dfaTonfaMap.put(dfaMap.dfaStatusId, dfaMap);
					untagedStates.push(newStatus.getStateId());
					this.putState(newStatus);
					currentState.addTransition(range, newStatus.getStateId());	
					
					for(StateID stateId : temp){
						if(nfa.isEndState(stateId)){
							this.endStateIds.add(newStatus.getStateId());
							break;
						}
					}
				}				
			}
		}
		
		// clean up
		Iterator<Entry<StateID, DFAToNFAMap>> dfaTonfaMapIt = dfaTonfaMap.entrySet().iterator();
		while(dfaTonfaMapIt.hasNext()){
			dfaTonfaMapIt.next().getValue().clear();
			dfaTonfaMapIt.remove();
		}
		
		DFA dfa = new DFA(this.startState, this.statesMap, this.endStateIds);
		dfa.minimize();
		this.cleanup();
		return dfa;
	}
	
	private final class DFAToNFAMap {
		private StateID dfaStatusId;
		private Set<StateID> nfaStatusSet;
		
		private DFAToNFAMap(StateID dfaStatusId){
			this.dfaStatusId = dfaStatusId;
			this.nfaStatusSet = new HashSet<StateID>();
		}
		
		private final void addNFAState(Collection<StateID> nfaStatus){
			this.nfaStatusSet.addAll(nfaStatus);
		}
		
		private final boolean contains(Collection<StateID> nfas){
			if(this.nfaStatusSet.size() != nfas.size()){
				return false;
			}
			for(StateID id : nfas){
				if(!this.nfaStatusSet.contains(id)){
					return false;
				}
			}
			return true;
		}
		
		private final void clear(){
			this.nfaStatusSet.clear();
		}
		
		@Override
		public String toString(){
			return "DFA ID: " + this.dfaStatusId + " " + this.nfaStatusSet.toString();
		}
	}


}
