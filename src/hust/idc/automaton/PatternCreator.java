package hust.idc.automaton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * ��DFA����������ʽ�Ĺ�����
 * @author WangCong
 *
 */
final class PatternCreator {
	private final class TempStatus{
		private StateID stateId;
		private final Map<StateID, String> transitionMap;
		private final Set<StateID> prevIdSet;
		
		private TempStatus(StateID statusId){
			this.stateId = statusId;
			this.transitionMap = new HashMap<StateID, String>();
			this.prevIdSet = new HashSet<StateID>();
		}
		
		private TempStatus(DFAState status){
			this.stateId = status.getStateId();
			this.transitionMap = new HashMap<StateID, String>();
			this.prevIdSet = new HashSet<StateID>();

			Iterator<Entry<CharRange, StateID>> transIt = status.getTransitionIterator();
			while(transIt.hasNext()){
				Entry<CharRange, StateID> entry = transIt.next();
				this.addTransition(entry.getKey(), entry.getValue());
			}
		}
		
		private final void addTransition(CharRange range, StateID transitId){
			if(range == null || range.size() <= 0)
				return;
			this.addTransition(range.toRegexString(), transitId);
		}
		
		private final void addTransition(String regex, StateID transitId){
			if(regex == null || regex.length() <= 0)
				return;
			this.transitionMap.put(transitId, regexOr(this.transitionMap.get(transitId), regex));
		}
		
		private final boolean hasTransiteToSelf(){
			return this.transitionMap.containsKey(this.stateId);
		}
		
		private final String getSelfRegex(){
			return this.transitionMap.get(this.stateId);
		}
		
		@Override
		public final String toString(){
			return "Status Id: " + this.stateId + ", " 
				   + "prev: " + this.prevIdSet.toString() 
				   + " succession: " + this.transitionMap.toString() + "\n";
		}
	}
	
	private final DFA dfa;
	private String regex;
	private final Map<StateID, TempStatus> statesMap;
	private Set<StateID> endStateIds;
	private Set<StateID> statusSet;
	
	private static final StateID TEMP_END_STATUS_ID = new StateID();
	
	private static final Map<String, String> replaceMap;
	
	static {
		replaceMap = setReplaceMap();
	}
	/**
	 * ����һ������ʽ������������
	 * @param automaton��Ҫ����������ʽ��DFA
	 * @throws IllegalArgumentException
	 * ��automaton == null����automaton��״̬������ֹ״̬��Ϊ��ʱ�׳�IllegalArgumentException
	 * ���׳��쳣�����automaton��ʾ�����򼯷ǿռ�����˲���֤�ܳɹ������ǿյ��������?
	 */
	protected PatternCreator(DFA dfa) throws IllegalArgumentException {
		if(dfa == null || dfa.isEmpty())
			throw new IllegalArgumentException("empty finite automaton");
		this.dfa = dfa;
		this.regex = null;
		this.statesMap = new HashMap<StateID, TempStatus>();
		this.statusSet = new HashSet<StateID>();
		this.endStateIds = new HashSet<StateID>();
	}

	private final void reset(){
		this.regex = null;
		this.endStateIds.clear();
		this.statusSet.clear();
		this.statesMap.clear();
	}
	
	private final void initStateMap() {
		// TODO Auto-generated method stub
		this.endStateIds = new HashSet<StateID>(this.dfa.getEndStateIds());
		this.statusSet = new HashSet<StateID>(this.dfa.getStatesMap().keySet());
		this.statusSet.remove(this.dfa.getStartStateId());
		
		Iterator<DFAState> faStatusIt = this.dfa.getStateIterator();
		while(faStatusIt.hasNext()){
			StateID stateId = faStatusIt.next().getStateId();
			this.statesMap.put(stateId, new TempStatus(stateId));
		}
		
		faStatusIt = this.dfa.getStateIterator();
		while(faStatusIt.hasNext()){
			DFAState state = faStatusIt.next();
			StateID stateId = state.getStateId();
			TempStatus tempStatus = this.statesMap.get(stateId);
			
			Iterator<Entry<CharRange, StateID>> transIt = state.getTransitionIterator();
			while(transIt.hasNext()){
				Entry<CharRange, StateID> entry = transIt.next();
				StateID transId = entry.getValue();
				tempStatus.addTransition(entry.getKey(), transId);
				
				if(stateId.equals(transId))
					continue;
				TempStatus transStatus = this.statesMap.get(transId);
				transStatus.prevIdSet.add(stateId);
				this.statesMap.put(transId, transStatus);
			}
			this.statesMap.put(stateId, tempStatus);
		}
		
		// ����һ����ʱ��ֹ״̬���ϲ����г���Ϊ0����ֹ״̬������û��ת������
		TempStatus endStatus = new TempStatus(TEMP_END_STATUS_ID);
		this.statesMap.put(endStatus.stateId, endStatus);
	}

	/**
	 * ����������ʽ
	 * @return����DFA�ȼ۵�������ʽ�����Ե����մ�������ʽ������ʧ�ܷ���null
	 */
	protected Pattern createPattern(){
		if(this.regex == null || this.regex.length() <= 0)
			this.generateRegex();
		try {
			return Pattern.compile(this.regex);
		} catch(Exception e){
			return null;
		}
	}
	
	private void generateRegex() {
		// TODO Auto-generated method stub
		if(this.dfa == null || this.dfa.isEmpty())
			return;

		// ��ʼ������
		this.reset();
		this.initStateMap();
		
		Iterator<StateID> statusIt = this.statusSet.iterator();
		while(statusIt.hasNext()){
			StateID statusId = statusIt.next();
			this.stateEliminate(statusId);
			statusIt.remove();
		}
		
		// �����Զ���ֻʣ�¿�ʼ�ڵ����ʱ��ֹ�ڵ������ڵ�?
		// ���ؿ�ʼ�ڵ�ָ����ʱ��ֹ�ڵ���������?
		TempStatus startStatus = this.statesMap.get(this.dfa.getStartStateId());
		String regex1 = startStatus.transitionMap.get(TEMP_END_STATUS_ID);
		
		// ���ʼ�ڵ���ָ���Լ�ת������?
		if(startStatus.hasTransiteToSelf()){
			if(this.dfa.isEndState(this.dfa.getStartStateId())){
				if(regex1 != null){
					String regex2 = regexRepeat(startStatus.getSelfRegex());
					this.regex = regex2 + "|" + regexJoin(regex2, regex1);
				}
				else
					this.regex = addBracket(startStatus.getSelfRegex()) + "*";
			} else{
				if(regex1 != null){
					String regex2 = regexRepeat(startStatus.getSelfRegex());
					this.regex = regexJoin(regex2, regex1);
				} else{
					this.regex = null;
				}
			}
		} else {
			this.regex = regex1;
		}
		
		this.regex = simplifyRegex(this.regex);
	}
	
	private void stateEliminate(StateID stateId){
		TempStatus nowState = this.statesMap.get(stateId);
		// ����ǰ��ڵ�?
		Iterator<StateID> prevIt = nowState.prevIdSet.iterator();
		while(prevIt.hasNext()){
			StateID prevId = prevIt.next();
			// ȥ��ָ��ǰ��ڵ��еķ���ָ��?
			prevIt.remove();
			TempStatus prevStatus = this.statesMap.get(prevId);
			if(prevStatus == null)
				continue;
			String prevRegex = prevStatus.transitionMap.get(stateId);
			
			if(nowState.hasTransiteToSelf()){
				prevRegex = regexJoin(prevRegex, nowState.getSelfRegex()) + "*";
			}
			
			if(this.endStateIds.contains(stateId)){
				// ��ǰ�ڵ�����ֹ�ڵ㣬����ǰ��ڵ�����ʱ��ֹ�ڵ����ϵ
				prevStatus.addTransition(prevRegex, TEMP_END_STATUS_ID);
			}
			
			// �����̽��?
			Iterator<StateID> succIt = nowState.transitionMap.keySet().iterator();
			while(succIt.hasNext()){
				StateID succId = succIt.next();
				if(succId.equals(stateId))
					continue;
				TempStatus succStatus = this.statesMap.get(succId);
				if(succStatus == null)
					continue;
				String succRegex = nowState.transitionMap.get(succId);
				
				// ���ǰ�������̽���ֱ��ת������?
				prevStatus.addTransition(regexJoin(prevRegex, succRegex), succId);
				// Ϊ��̽��ĺ�̽����ӷ���ָ��
				succStatus.prevIdSet.add(prevId);
			}
			// ȥ��ǰ������ָ�������ת������?
			prevStatus.transitionMap.remove(stateId);
		}
		this.statesMap.remove(stateId);
	}
	
	private static String simplifyRegex(final String regex){
		if(regex == null || regex.length() <= 0)
			return regex;
		
		StringBuilder temp = new StringBuilder(regex);
		Iterator<String> replaceIt = replaceMap.keySet().iterator();
		while(replaceIt.hasNext()){
			String beReplaced = replaceIt.next();
			String replace = replaceMap.get(beReplaced);
			for(int i = 0; i < temp.length(); ){
				int start = temp.indexOf(beReplaced, i);
				int end = start + beReplaced.length();
				if(start == -1)
					break;
				try{
					if(temp.charAt(end) == '*'){
						i = end + 1;
						break;
					}
					if(temp.charAt(start - 1) == '[' && temp.charAt(end) == ']'){
						--start; 
						++end;
					} else if(temp.charAt(start - 1) == '(' && temp.charAt(end) == ')'){
						--start; 
						++end;
					}
				}catch(Exception e){}
				
				temp.replace(start, end, replace);
				i = start + replace.length();
			}
		}
		return temp.toString();
	}
	
	private static String regexOr(String regex1, String regex2){
		if(regex1 == null || 0 == regex1.length())
			return regex2;
		if(regex2 == null || 0 == regex2.length())
			return regex1;
		
		return regex1 + "|" + regex2;
	}
	
	private static String regexJoin(String regex1, String regex2){
		return addBracket(regex1) + addBracket(regex2);
	}
	
	private static String regexRepeat(String regex){
		if(regex == null || regex.length() == 0)
			return null;
		if(regex.length() <= 1)
			return regex + "*";
		
		if('(' == regex.charAt(0) && ')' == regex.charAt(regex.length() - 1)){
			int bracketCount = 0;
			boolean esc = false, needBracket = false;
			for(int i = 1; i < regex.length(); ++i){
				switch(regex.charAt(i)){
				case CharacterEscaper.ESCAPE_CHAR: 
					esc = !esc;
					break;
				case '(': 
					if(!esc)
						++bracketCount;
					break;
				case ')':
					if(!esc){
						if(bracketCount == 0){
							needBracket = (i != regex.length() - 1);
							break;
						}
						--bracketCount;
					}
					break;
				default:
					esc = false;
					break;
				}
			}
			
			return needBracket ? "(" + regex + ")*" : regex + "*";
		} else {
			return "(" + regex + ")*";
		}
	}
	
	private static final String addBracket(String regex){
		if(!regex.contains("|"))
			return regex;
		else if(!regex.contains("(") && !regex.contains("["))
			return "(" + regex + ")";
		
		boolean needBracket = false, esc = false;
		int squareBracket = 0, rountBracket = 0;
		for(int i = 0; i < regex.length() && !needBracket; ++i){
			switch(regex.charAt(i)){
			case '|':
				if(!esc)
					needBracket = (squareBracket == 0 && rountBracket == 0);
				esc = false;
				break;
			case '(':
				if(!esc)
					++rountBracket;
				esc = false;
				break;
			case ')':
				if(!esc)
					--rountBracket;
				esc = false;
				break;
			case '[':
				if(!esc)
					++squareBracket;
				esc = false;
				break;
			case ']':
				if(!esc)
					--squareBracket;
				esc = false;
				break;
			case CharacterEscaper.ESCAPE_CHAR: 
				esc = !esc;
				break;
			default:
				esc = false;
				break;
			}
		}
		
		if(needBracket)
			return "(" + regex + ")";
		else
			return regex;
	}
	
	@SuppressWarnings("unused")
	private static List<CharSequence> splitRegex(final CharSequence sequence){
		List<CharSequence> splits = new ArrayList<CharSequence>();
		int roundBracket = 0, squareBracket = 0;
		int start = 0;
		boolean esc = false;
		for(int i = 0; i < sequence.length(); ++i){
			switch(sequence.charAt(i)){
			case '(':
				if(!esc)
					++roundBracket;
				esc = false;
				break;
			case ')':
				if(!esc)
					--roundBracket;
				esc = false;
				break;
			case '[':
				if(!esc)
					++squareBracket; 
				esc = false;
				break;
			case ']':
				if(!esc)
					--squareBracket; 
				esc = false;
				break;
			case '|':
				if(!esc && squareBracket == 0 && roundBracket == 0){
					splits.add(sequence.subSequence(start, i));
					start = i + 1;
				}
				esc = false;
				break;
			case CharacterEscaper.ESCAPE_CHAR:
				esc = !esc;
				break;
			default: break;
			}
		}
		splits.add(sequence.subSequence(start, sequence.length()));
		return splits;
	}
	
	private static final Map<String, String> setReplaceMap() {
		// TODO Auto-generated method stub
		Map<String, String> map = new HashMap<String, String>();
		
		map.put("[\\t-\\r]| ", "\\s");
//		map.put("( |[\\t-\\r])", "\\s");
		map.put("[\\u000e-\\u001f]|[\\u0001-\\u0008]|[!-\\uffff]", "\\S");
		map.put("\\p{Upper}|\\p{Lower}", "\\p{Alpha}");
		map.put("\\p{Upper}|\\d|\\p{Lower}", "\\p{Alnum}");
		map.put("\\p{Upper}|_|\\p{Lower}", "\\w");
		map.put("[:-@]|`|[\\u0001-/]|[\\[-\\^]|[\\{-\\uffff]", "\\W");
		map.put("[\\u0001-\\t]|[\\u000b-\\uffff]", ".");
//		map.put("[\\u000b-\\uffff]|[\\u0001-\\t]", ".");
		map.put("[\\u0001-/]|[:-\\uffff]", "\\D");
		map.put("\\d|[a-f]|[A-F]", "\\p{XDigit}");
		map.put("[:-@]|[\\{-~]|[\\[-`]|[!-/]", "\\p{Punct}");
		map.put("[\\u0001-\\u001f]|\\u007f", "\\p{Cntrl}");
		return map;
	}

}
