package hust.idc.automaton;

import hust.idc.util.Pair;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.Map.Entry;

class DFAMinimizer {
	private DFA dfa;
	private Map<Pair<StateID, StateID>, Boolean> markMap = new HashMap<Pair<StateID, StateID>, Boolean>();
	private Map<StateID, StateID> partitionMap = new HashMap<StateID, StateID>();
	
	protected DFAMinimizer(DFA dfa){
		this.dfa = dfa;
	}
	
	private void cleanup(){
		this.markMap.clear();
		this.partitionMap.clear();
	}
	
	protected boolean minimize(){
		if(dfa == null || dfa.isEmpty())
			return true;
		
		this.initMarkMap();
		this.markEqualState();

		if (!markMap.isEmpty()) {
			this.initPartitionMap();

			Iterator<Pair<StateID, StateID>> markIt = markMap.keySet().iterator();
			while (markIt.hasNext()) {
				Pair<StateID, StateID> pair = markIt.next();
				StateID partitionId = partitionMap.get(pair.getFirst());
				StateID changeId = partitionMap.get(pair.getSecond());

				if (this.dfa.isStartState(changeId)) {
					changeId = partitionId;
					partitionId = this.dfa.getStartStateId();
				}

				partitionMap.put(pair.getFirst(), partitionId);
				partitionMap.put(pair.getSecond(), partitionId);

				// 合并划分
				if (partitionMap.containsValue(changeId)) {
					Iterator<StateID> partitionIt = partitionMap.keySet()
							.iterator();
					while (partitionIt.hasNext()) {
						StateID stateId = partitionIt.next();
						StateID statePart = partitionMap.get(stateId);
						if (statePart.equals(changeId))
							partitionMap.put(stateId, partitionId);
					}
				}

				Set<StateID> partitionSet = new HashSet<StateID>(
						partitionMap.values());
				Iterator<StateID> partIt = partitionSet.iterator();
				while (partIt.hasNext()) {
					StateID partId = partIt.next();
					if (partId.equals(DFAState.FAILED_STATE_ID))
						continue;

					DFAState represent = this.dfa.getState(partId);
					Iterator<Entry<CharRange, StateID>> transIt = represent
							.getTransitionIterator();
					while (transIt.hasNext()) {
						Entry<CharRange, StateID> entry = transIt.next();
						
						// use method entry.setValue the dfa state won't combine the char ranges that
						// transit to the same state
						entry.setValue(partitionMap.get(entry.getValue()));
					}
					represent.combineTransitions();
				}
			}
		}

		// 对状态集剪枝
		this.pruningStates();
		this.cleanup();
		return true;
	}
	
	private void initMarkMap(){
		if(this.markMap == null)
			this.markMap = new HashMap<Pair<StateID, StateID>, Boolean>();
		
		Iterator<DFAState> stateIt1 = this.dfa.getStateIterator();
		while (stateIt1.hasNext()) {
			StateID id1 = stateIt1.next().getStateId();
			if (id1.equals(DFAState.FAILED_STATE_ID))
				continue;

			Iterator<DFAState> statusIt2 = this.dfa.getStateIterator();
			while (statusIt2.hasNext()) {
				StateID id2 = statusIt2.next().getStateId();
				if (id1.equals(id2))
					continue;

				Boolean mark = (this.dfa.isEndState(id1) == this.dfa
						.isEndState(id2));
				markMap.put(
						new Pair<StateID, StateID>(min(id1, id2), max(id1, id2)),
						mark);
			}

			markMap.put(new Pair<StateID, StateID>(DFAState.FAILED_STATE_ID,
					id1), Boolean.FALSE);
		}
	}
	
	private void markEqualState(){
		while (true) {
			boolean flag = true;
			Iterator<Pair<StateID, StateID>> markIt = markMap.keySet()
					.iterator();
			while (markIt.hasNext()) {
				Pair<StateID, StateID> pair = markIt.next();
				if (!markMap.get(pair))
					continue;

				if (this.dividable(this.dfa.getState(pair.getFirst()),
						this.dfa.getState(pair.getSecond()))) {
					flag = false;
					markMap.put(pair, Boolean.FALSE);
				}
			}
			if (flag)
				break;
		}
		
		Iterator<Pair<StateID, StateID>> markIt = markMap.keySet().iterator();
		while (markIt.hasNext()) {
			if (!markMap.get(markIt.next()))
				markIt.remove();
		}
	}
	
	private void initPartitionMap(){
		if(this.partitionMap == null)
			this.partitionMap = new HashMap<StateID, StateID>();
		
		Iterator<DFAState> stateIt = this.dfa.getStateIterator();
		while (stateIt.hasNext()) {
			StateID stateId = stateIt.next().getStateId();
			if (stateId.equals(DFAState.FAILED_STATE_ID))
				continue;
			partitionMap.put(stateId, stateId);
		}
		partitionMap.put(DFAState.FAILED_STATE_ID, DFAState.FAILED_STATE_ID);
	}

	private boolean dividable(DFAState state1, DFAState state2) {
		Set<Character> sigma = state1.getStartPoints();
		sigma.addAll(state2.getStartPoints());

		Iterator<Character> sigmaIt = sigma.iterator();
		while (sigmaIt.hasNext()) {
			char ch = sigmaIt.next();
			StateID transId1 = state1.getTransition(ch);
			StateID transId2 = state2.getTransition(ch);

			if (transId1.equals(transId2))
				continue;
			Pair<StateID, StateID> pair = new Pair<StateID, StateID>(min(
					transId1, transId2), max(transId1, transId2));
			if (!markMap.containsKey(pair) || !markMap.get(pair))
				return true;
		}
		return false;
	}

	private void pruningStates() {
		if (this.dfa.isEmpty())
			return;

		Map<StateID, Integer> prunnedMap = new HashMap<StateID, Integer>();
		Map<StateID, Set<StateID>> statesReverseGraph = new HashMap<StateID, Set<StateID>>();

		Iterator<DFAState> statesIt = this.dfa.getStateIterator();
		while (statesIt.hasNext()) {
			StateID stateId = statesIt.next().getStateId();
			prunnedMap.put(stateId, UNREACHABLE);
			statesReverseGraph.put(stateId, new HashSet<StateID>());
		}

		if (deepFirstSearchMark(this.dfa.getStatesMap(), this.dfa.getEndStateIds(), this.dfa.getStartStateId(),
				new Stack<StateID>(), prunnedMap, statesReverseGraph) != END_REACHABLE) {
			this.dfa.getStatesMap().clear();
			this.dfa.getEndStateIds().clear();
			this.dfa.getStatesMap().put(this.dfa.getStartStateId(), new DFAState(this.dfa.getStartStateId()));
			return;
		}

		Iterator<Entry<StateID, Integer>> prunIt = prunnedMap.entrySet().iterator();
		while (prunIt.hasNext()) {
			Entry<StateID, Integer> prunEntry = prunIt.next();
			StateID stateId = prunEntry.getKey();
			int statusPrunMark = prunEntry.getValue().intValue();
			if (statusPrunMark != END_REACHABLE
					&& statusPrunMark != CAN_BE_RETURNED
					&& statusPrunMark != TRANS_PRUNNED) {

				this.dfa.getStatesMap().remove(stateId);
				this.dfa.getEndStateIds().remove(stateId);

				Set<StateID> prevs = statesReverseGraph.get(stateId);
				Iterator<StateID> prevsIt = prevs.iterator();
				while (prevsIt.hasNext()) {
					StateID prevsId = prevsIt.next();
					int prevPrunMark = prunnedMap.get(prevsId).intValue();
					if (prevPrunMark != END_REACHABLE
							&& statusPrunMark != CAN_BE_RETURNED)
						continue;

					DFAState state = this.dfa.getState(prevsId);
					Iterator<Entry<CharRange, StateID>> transIt = state
							.getTransitionIterator();
					while (transIt.hasNext()) {
						Entry<CharRange, StateID> entry = transIt.next();
						StateID transId = entry.getValue();
						int transPrunMark = prunnedMap.get(transId).intValue();
						if (transPrunMark != END_REACHABLE
								&& transPrunMark != CAN_BE_RETURNED
								&& transPrunMark != TRANS_PRUNNED) {
							transIt.remove();
						}
					}
					// when traverse a hash map with an iterator, we can replace an old value with 
					// a new one use the put method. But we cannot put an value whose key does not 
					// exist yet in the map 
					prunnedMap.put(state.getStateId(), TRANS_PRUNNED);
				}
			}
		}
		
		prunnedMap.clear();
		Iterator<Entry<StateID, Set<StateID>>> statesReverseGraphIt = statesReverseGraph.entrySet().iterator();
		while(statesReverseGraphIt.hasNext()){
			statesReverseGraphIt.next().getValue().clear();
			statesReverseGraphIt.remove();
		}
	}

	private static int deepFirstSearchMark(
			final Map<StateID, DFAState> statesMap,
			final Set<StateID> endStateIds, final StateID startId,
			final Stack<StateID> prevStack,
			final Map<StateID, Integer> markMap,
			final Map<StateID, Set<StateID>> stateReverseGraph) {
		// TODO Auto-generated method stub
		try {
			int mark = START_REACHABLE;
			markMap.put(startId, START_REACHABLE);
			DFAState status = statesMap.get(startId);
			if (status == null)
				return UNREACHABLE;

			if (endStateIds.contains(startId)) {
				markMap.put(startId, END_REACHABLE);
				mark = END_REACHABLE;
			} else if (prevStack.contains(startId)) {
				mark = CAN_BE_RETURNED;
			}

			prevStack.push(startId);
			Iterator<Entry<CharRange, StateID>> transIt = status
					.getTransitionIterator();
			while (transIt.hasNext()) {
				Entry<CharRange, StateID> entry = transIt.next();
				StateID transId = entry.getValue();
				if (markMap.get(transId).equals(UNREACHABLE)) {
					mark = combineMark(
							mark,
							deepFirstSearchMark(statesMap, endStateIds,
									transId, prevStack, markMap,
									stateReverseGraph));
				} else
					mark = combineMark(mark, markMap.get(transId));

				Set<StateID> prevSet = stateReverseGraph.get(transId);
				if (prevSet == null)
					prevSet = new HashSet<StateID>();
				prevSet.add(startId);
				stateReverseGraph.put(transId, prevSet);
			}
			markMap.put(startId, mark);
			prevStack.pop();
			return mark;
		} catch (Exception e) {
			return UNREACHABLE;
		}
	}

	private static int combineMark(int mark1, int mark2) {
		return Math.max(mark1, mark2);
	}

	private static StateID min(StateID id1, StateID id2) {
		return id1.compareTo(id2) <= 0 ? id1 : id2;
	}

	private static StateID max(StateID id1, StateID id2) {
		return id1.compareTo(id2) >= 0 ? id1 : id2;
	}
	
	private static final int UNREACHABLE = 0;
	private static final int START_REACHABLE = 1;
	private static final int CAN_BE_RETURNED = 2;
	private static final int END_REACHABLE = 3;
	private static final int TRANS_PRUNNED = 4;

}
