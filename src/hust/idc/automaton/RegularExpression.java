package hust.idc.automaton;

import java.util.ArrayList;
import java.util.List;

class RegularExpression {
	private final List<RegexpElement> regularExpression;
	private boolean preProcessed;
	
	protected RegularExpression(){
		this.regularExpression = new ArrayList<RegexpElement>();
		this.preProcessed = false;
	}
	
	protected final List<RegexpElement> getRegularExpression(){
		if(!this.preProcessed)
			this.preProcess();
		return this.regularExpression;
	}
	
	/**
	 * �Ը���û�������һ���򵥵���֤�����Ƿ��ϺϷ������ʽ��Լ��
	 * @param source
	 * @throws IllegalArgumentException
	 */
	private void validate() throws IllegalArgumentException {
		if(this.isEmpty())
			return;
		
		if(this.regularExpression.get(0).isEndElement()){
			throw ErrorHandler.illegalStart();
		}
		
		int oprandCounter = 0;			
		for(int i = 0; i < this.regularExpression.size(); ++i){
			if(this.regularExpression.get(i).isOperand()){
				++oprandCounter;
			}
			
			if(!this.regularExpression.get(i).isOperator() && !this.regularExpression.get(i).isOperand()){
				throw ErrorHandler.undefinedSymbol(this.regularExpression.get(i));				                           
			}
						
			if(this.regularExpression.get(i).isJoinOperator() && this.regularExpression.get(i + 1).isJoinOperator()){
				throw ErrorHandler.illegalOperatorSequence(this.regularExpression.get(i));
			}
			
			if(this.regularExpression.get(i).isOrOperator() && this.regularExpression.get(i+1).isOrOperator()){
				throw ErrorHandler.illegalOperatorSequence(this.regularExpression.get(i));
			}
		}
		
		if(0 == oprandCounter)
			throw ErrorHandler.nonOperand();
	}

	/**
	 * Validate the source and add dot.
	 * �ȼ�Ԥ���?�ж������ַ��Ƿ�Ϸ����ټӵ�?
	 * @param source to be preProcessed string.
	 * @return the processed string
	 */
	private void preProcess(){
		try{
			this.validate();
		}catch(IllegalArgumentException e){
			e.printStackTrace();
		}
		
		this.addJoinElement();
		this.preProcessed = true;
	}
	

	/**
	 * �Ը��ַ����ӵ����?
	 * @param source
	 * @return
	 */
	private void addJoinElement() {
		if(this.isEmpty())
			return;
		
		List<RegexpElement> result=new ArrayList<RegexpElement>();
		//char[] input=source.toCharArray();
		result.add(this.regularExpression.get(0));
		int currentAppended=0;
		int next = currentAppended + 1;
		while(next < this.regularExpression.size() && !this.regularExpression.get(next).isEndElement()){
			if(this.regularExpression.get(currentAppended).needJoinBetween(this.regularExpression.get(next))){
				result.add(RegexpElementFactory.getJoinOperator());
			}
			result.add(this.regularExpression.get(next));
			currentAppended++;
			next=currentAppended+1;					
		}
		result.add(RegexpElementFactory.getEndOperator());
		this.regularExpression.clear();
		this.regularExpression.addAll(result);
	}
	
	protected final void addOperand(CharRange range){
		this.addElement(RegexpElementFactory.getOprand(range));
	}
	
	protected final void addOperand(char character){
		this.addElement(RegexpElementFactory.getOprand(character));
	}
	
	protected final void addOperator(char operator){
		this.addElement(RegexpElementFactory.getOperator(operator));
	}
	
	protected final void addElement(RegexpElement element){
		if(element == null)
			return ;
		this.preProcessed = false;
		this.regularExpression.add(element);
	}

	@Override
	public String toString(){	
		return regularExpression.toString();
	}
	
	protected final boolean isEmpty(){
		return this.regularExpression == null || 
			this.regularExpression.isEmpty();
	}
	
	protected final void clear(){
		this.regularExpression.clear();
		this.preProcessed = true;
	}
	
	/**
	 * ������
	 * @author tjw
	 *
	 */
	private static final class ErrorHandler{
		
		static final IllegalArgumentException undefinedSymbol(RegexpElement elem){
			return new IllegalArgumentException("--reasons: the symbol \""
					+ elem.toRegexString() +"\" is not defined in the RegularExpression");
		}
		
		static final IllegalArgumentException nonOperand(){
			return new IllegalArgumentException("--reasons: there is no oprand in your RegualrExpression," +
					"go and check it.");
		}
		
		static final IllegalArgumentException illegalStart(){
			return new IllegalArgumentException("--reasons: RegualrExpression cannot start with \"#\" " +
					",go and check it.");
		}
		
		static final IllegalArgumentException illegalOperatorSequence(RegexpElement elem){
			return new IllegalArgumentException("--reasons: RegualrExpression cannot contains \"" + elem.toRegexString() +"\" sequence, go and check it.");
		}
	}

}
