package hust.idc.automaton;


import hust.idc.util.Pair;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Stack;
import java.util.regex.Pattern;
/**
 * NFA��
 * ����ĸ������ΪCharRange���͵�����
 * ���ַ���'/0'��ʾ
 * ��Ҫ��NFA�����㷨���μ���ݽṹ��?��ε���棬���ʽ��ֵ��һ��?
 * @author tjw
 */
class NFA implements Cloneable {
	private Map<StateID, NFAState> statesMap;
	private StateID startStateId;//��ʼ״̬
	private Set<StateID> endStateIds;//����״̬��
	
	protected NFA(){
		this(new NFAState());
	}
	
	protected NFA(NFAState startState){
		this.statesMap = new HashMap<StateID, NFAState>();
		this.endStateIds = new HashSet<StateID>();
		if(startState == null){
			NFAState start = new NFAState();
			this.statesMap.put(start.getStateId(), start);
			this.startStateId = start.getStateId();
		} else {
			this.statesMap.put(startState.getStateId(), startState);
			this.startStateId = startState.getStateId();
		}
	}
	
	protected NFA(StateID startStateId, Map<StateID, NFAState> statesMap, Set<StateID> endStateIds){
		this.startStateId = startStateId;
		this.statesMap = statesMap;
		this.endStateIds = endStateIds;
	}
	
	private NFA copy(){
		StateID newStartId = null;
		Map<StateID, NFAState> newStatesMap = new HashMap<StateID, NFAState>();
		Set<StateID> newEndStateIds = new HashSet<StateID>();
		Map<StateID, StateID> stateIdMap = new HashMap<StateID, StateID>();
		
		Iterator<NFAState> stateIt = this.getStateIterator();
		while(stateIt.hasNext()){
			NFAState state = stateIt.next();
			NFAState newState = new NFAState();
			
			newStatesMap.put(newState.getStateId(), newState);
			stateIdMap.put(state.getStateId(), newState.getStateId());
			
			if(this.isEndState(state.getStateId()))
				newEndStateIds.add(newState.getStateId());
			if(this.isStartState(state.getStateId()))
				newStartId = newState.getStateId();
		}
		
		stateIt = this.getStateIterator();
		while(stateIt.hasNext()){
			NFAState state = stateIt.next();
			NFAState newState = newStatesMap.get(stateIdMap.get(state.getStateId()));
			
			Iterator<Pair<CharRange, StateID>> transIt = state.getTransitionIterator();
			while(transIt.hasNext()){
				Pair<CharRange, StateID> transEdge = transIt.next();
				newState.addTransition(transEdge.getFirst(), stateIdMap.get(transEdge.getSecond()));
			}
		}
		
		stateIdMap.clear();
		return new NFA(newStartId, newStatesMap, newEndStateIds);
	}
	
	protected void union(NFA nfa){
		this.unionInternal(nfa.copy());
	}
	
	protected void union(DFA dfa){
		NFABuilder builder = new NFABuilder();
		NFA nfa = builder.getInstance(dfa);
		this.unionInternal(nfa);
		nfa.clear();
	}
	
	protected void union(Pattern pattern){
		NFABuilder builder = new NFABuilder();
		NFA nfa = builder.getInstance(pattern);
		this.unionInternal(nfa);
		nfa.clear();
	}
	
	private void unionInternal(NFA nfa){
		if(nfa == null || this == nfa || this.isEmpty() || nfa.isEmpty())
			return ;
		
		NFAState newStart = new NFAState();
		newStart.addNullTransition(startStateId);
		newStart.addNullTransition(nfa.getStartStateId());
		this.statesMap.put(newStart.getStateId(), newStart);
		this.startStateId = newStart.getStateId();
		this.statesMap.putAll(nfa.statesMap);
		this.endStateIds.addAll(nfa.endStateIds);
	}
	
	protected DFA determinize(){
		if(this.isEmpty())
			return DFA.EMPTY_DFA;
		NFADeterminizer determinizer = new NFADeterminizer(this);
		return determinizer.determinize();
	}
	
	private void clear(){
		// clear the states map 
		// but do not dispose states
		this.startStateId = null;
		this.statesMap.clear();
		this.endStateIds.clear();
	}
	
	protected void dispose(){
		// clear map and dispose all states
		Iterator<Entry<StateID, NFAState>> stateIt = this.statesMap.entrySet().iterator();
		while(stateIt.hasNext()){
			stateIt.next().getValue().dispose();
			stateIt.remove();
		}
		this.statesMap = null;
		this.endStateIds.clear();
		this.endStateIds = null;
	}

	protected final StateID getStartStateId(){
		return this.startStateId;
	}
	
	protected final NFAState getStartState(){
		return this.statesMap.get(this.startStateId);
	}
	
	protected final Iterator<NFAState> getStateIterator(){
		return new Iterator<NFAState>(){
			Iterator<Entry<StateID, NFAState>> iterator = statesMap.entrySet().iterator();

			@Override
			public boolean hasNext() {
				// TODO Auto-generated method stub
				return iterator.hasNext();
			}

			@Override
			public NFAState next() {
				// TODO Auto-generated method stub
				return iterator.next().getValue();
			}

			@Override
			public void remove() {
				// TODO Auto-generated method stub
				iterator.remove();
			}
		};
	}
	
	protected final Iterator<NFAState> getEndStateIterator(){
		return new Iterator<NFAState>(){
			Iterator<StateID> iterator = endStateIds.iterator();

			@Override
			public boolean hasNext() {
				// TODO Auto-generated method stub
				return iterator.hasNext();
			}

			@Override
			public NFAState next() {
				// TODO Auto-generated method stub
				return statesMap.get(iterator.next());
			}

			@Override
			public void remove() {
				// TODO Auto-generated method stub
				iterator.remove();
			}
			
		};
	}
	
	private final NFAState getState(StateID stateId){
		return this.statesMap.get(stateId);
	}
	
	protected final List<Character> getStartPoints(){
		Set<Character> pointSet = new HashSet<Character>();
		for(Iterator<NFAState> stateIt = this.getStateIterator(); stateIt.hasNext(); ){
			NFAState state = stateIt.next();
			pointSet.addAll(state.getStartPoints());
		}

		List<Character> pointList = new ArrayList<Character>(pointSet);
		Collections.sort(pointList);
		return pointList;
	}
	
	protected final Set<StateID> getTransition(StateID startId, char ch){
		NFAState state = this.getState(startId);
		return state.getTransition(ch);
	}
	
	protected final Set<StateID> getTransition(Collection<? extends StateID> startIdSet, char ch){
		Set<StateID> transIdSet = new HashSet<StateID>();
		for(StateID id : startIdSet){
			NFAState nfaState = this.getState(id);
			if(nfaState == null)
				continue;
			transIdSet.addAll(nfaState.getTransition(ch));
		}
		return transIdSet;
	}
	
	protected final boolean isStartState(StateID stateId){
		return this.startStateId != null && this.startStateId.equals(stateId);
	}
	
	protected final boolean isEndState(StateID stateId){
		return this.endStateIds.contains(stateId);
	}
	
	protected final boolean isEmpty(){
		return this.startStateId == null || 
			this.statesMap.isEmpty() || 
			this.endStateIds.isEmpty() || 
			!this.statesMap.containsKey(this.startStateId);
	}
	
	/**
	 * ����ĳһ״̬�ġ��ա��հ�
	 * ��NFA��״̬�Ӽ�I�г����������������ա������ܵ�����κ�״�?
	 * @param stateId Ҫ����ձհ��״̬ID
	 * @return
	 */
	protected Set<StateID> nullClosure(StateID stateId){
		Stack<StateID> workspace = new Stack<StateID>();
		Set<StateID> result = new HashSet<StateID>();
		result.add(stateId);
		workspace.push(stateId);
		while(!workspace.isEmpty()){
			NFAState state = this.getState(workspace.pop());
			for(StateID transId : state.getNullTransition()){
				if(!result.contains(transId)){
					result.add(transId);
					workspace.push(transId);
				}
			}
		}
		return result;
	}
	
	protected Set<StateID> nullClosure(Collection<StateID> source){	
		Set<StateID> results = new HashSet<StateID>();
		for(StateID eachState : source){
			results.addAll(this.nullClosure(eachState));
		}
		return results;
	}
	
	/**
	 * ��ӡNFA����
	 */
	protected final void print(){
		System.out.println("״̬���ϣ� "+this.statesMap.keySet().toString());
		System.out.println("��ʼ״̬�� "+this.startStateId);
		System.out.println("��ֹ״̬�� "+this.endStateIds.toString());
		
		for(Iterator<NFAState> stateIt = this.getStateIterator(); stateIt.hasNext(); ){
			NFAState state = stateIt.next();
			System.out.println(state.toString());
		}
	}
	
	@Override
	protected NFA clone() {
		// TODO Auto-generated method stub
		return this.copy();
	}
	
}
