package hust.idc.automaton;

import hust.idc.util.Pair;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Map.Entry;

class NFAState {
	private List<Pair<CharRange, StateID>> transitions;
	private final StateID stateId;
	
	protected NFAState(){
		this(new StateID());
	}
	
	protected NFAState(StateID stateId){
		this.transitions = new ArrayList<Pair<CharRange, StateID>>();
		this.stateId = stateId;
	}
	
	protected NFAState(DFAState state){
		this.stateId = state.getStateId();
		this.transitions = new ArrayList<Pair<CharRange, StateID>>();
		Iterator<Entry<CharRange, StateID>> transIt = state.getTransitionIterator();
		while(transIt.hasNext()){
			Entry<CharRange, StateID> entry = transIt.next();
			this.addTransition(entry.getKey(), entry.getValue());
		}
	}
	
	protected final void dispose(){
		this.transitions.clear();
		this.transitions = null;
	}
	
	/**
	 * ���״̬ID
	 * @return
	 */
	protected final StateID getStateId(){	
		return stateId;
	}
	
	/**
	 * ����һ��ת������
	 * @param end  ����ߵ����?
	 * @param range  ������ַ�?
	 */
	protected final void addTransition(CharRange range, StateID endId){		
		transitions.add(new Pair<CharRange, StateID>(range, endId));
		//��¼���ӵı�
	}
	
	protected final void addTransition(char ch, StateID endId){
		this.addTransition(new CharRange(ch), endId);
	}
	
	protected final void addNullTransition(StateID endId){
		this.addTransition(CharRange.EPSILON_RANGE, endId);
	}
	
	protected final Iterator<Pair<CharRange, StateID>> getTransitionIterator(){
		return this.transitions.iterator();
	}

	/**
	 * ����Ը�״̬Ϊ��㾭��·��a���ܵ����״̬�ļ���?
	 * @param a	·��
	 * @return ArrayList<NFAState>
	 */
	protected final Set<StateID> getTransition(CharRange a){		
		Set<StateID> result = new HashSet<StateID>();
		Iterator<Pair<CharRange, StateID>> transIt = this.getTransitionIterator();
		while(transIt.hasNext()){
			Pair<CharRange, StateID> edge = transIt.next();
			if(edge.getFirst().equals(a)){
				result.add(edge.getSecond());
			}
		}
		return result;
	}
	
	protected final Set<StateID> getTransition(char ch){
		Set<StateID> result = new HashSet<StateID>();
		Iterator<Pair<CharRange, StateID>> transIt = this.getTransitionIterator();
		while(transIt.hasNext()){
			Pair<CharRange, StateID> edge = transIt.next();
			if(edge.getFirst().charIn(ch)){
				result.add(edge.getSecond());
			}
		}
		return result;
	}
	
	protected final Set<Character> getStartPoints(){
		Set<Character> pointset = new HashSet<Character>();
		pointset.add(CharRange.CHAR_MIN_VALUE);
		Iterator<Pair<CharRange, StateID>> transIt = this.getTransitionIterator();
		while(transIt.hasNext()){
			Pair<CharRange, StateID> edge = transIt.next();
			CharRange cr = edge.getFirst();
			if(!CharRange.EPSILON_RANGE.charIn(cr.getStart()))
				pointset.add(cr.getStart());
			if(cr.getEnd() < CharRange.CHAR_MAX_VALUE)
				pointset.add((char) (cr.getEnd() + 1));
		}
		return pointset;
	}
	
	protected final Set<StateID> getNullTransition(){
		return this.getTransition(CharRange.EPSILON_RANGE);
	}
    
	/**
	 * ͨ��˺�������Ϊ�ַ����
	 */
	@Override
	public final String toString(){
		StringBuilder builder = new StringBuilder("State ID: ");
		builder.append(stateId);
		builder.append("\n");
		Iterator<Pair<CharRange, StateID>> transIt = this.getTransitionIterator();
		while(transIt.hasNext()){
			Pair<CharRange, StateID> e = transIt.next();
			builder.append(this.stateId);
			builder.append(" To ");
			builder.append(e.getSecond());
			builder.append("  via ");
			builder.append(e.getFirst().toRegexString());
			builder.append("\n");
		}
		return builder.toString();
	}

}
