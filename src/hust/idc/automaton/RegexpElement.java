package hust.idc.automaton;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

abstract class RegexpElement {
	protected static final char LEFT_BRACKET_CHAR = '(';
	protected static final char RIGHT_BRACKET_CHAR = ')';
	protected static final char OR_REGULAR_CHAR = '|';
	protected static final char ONCE_OR_NONE_CHAR = '?';
	protected static final char CLOSUER_REGULAR_CHAR = '*';
	
	private static final Set<Character> supportedOperator;
	
	static{
		supportedOperator = new HashSet<Character>();
		supportedOperator.add(LEFT_BRACKET_CHAR);
		supportedOperator.add(RIGHT_BRACKET_CHAR);
		supportedOperator.add(OR_REGULAR_CHAR);
		supportedOperator.add(CLOSUER_REGULAR_CHAR);
		supportedOperator.add(ONCE_OR_NONE_CHAR);
	}
	
	protected RegexpElement(){ }
	
	protected static boolean operatorSupported(char operator){
		return supportedOperator.contains(operator);
	}
	
	protected abstract boolean isOperator();
	
	protected abstract boolean isOperand();
	
	protected abstract boolean isEndElement();
	
	protected abstract boolean isJoinOperator();
	
	protected abstract boolean isOrOperator();
	
	protected abstract boolean isClosureOperator();
	
	protected abstract boolean isOnceOrNoneOperator();
	
	protected abstract boolean isLeftBracket();
	
	protected abstract boolean isRightBracket();
	
	protected abstract CharRange getCharRange();
	
	protected abstract String toRegexString();
	
	protected int getPrecede(RegexpElement otherElem) throws IllegalArgumentException {
		throw illegalArgumentException(this, otherElem);
	}
	
	protected abstract boolean needJoinBetween(RegexpElement otherElem);
	
	protected static IllegalArgumentException illegalArgumentException(RegexpElement elem1, RegexpElement elem2){
		throw new IllegalArgumentException("--reasons: sorry the precede between \""+
				elem1.toRegexString() +"\" and \""+elem2.toRegexString()+"\" is not defined.");
	}

}

class RegexpOperator extends RegexpElement {
	private final int id;
	private final char operator;
	private static final Map<Character, RegexpOperator> regexpOperators;
	private static final RegexpOperator orOperator, joinOperator, endOperator;
	private static final int[][] precedeArray;
	
	private static final char JOIN_OF_REGULAR_CHAR = '.';
	private static final char END_OF_REGULAR_CHAR = '$';
	
	private static final int UNSUPPORTED_PRECEDE = Integer.MIN_VALUE;
	
	static {
		int count = 0;
		regexpOperators = new HashMap<Character, RegexpOperator>();
		regexpOperators.put(LEFT_BRACKET_CHAR, new RegexpOperator(LEFT_BRACKET_CHAR, count++));
		regexpOperators.put(RIGHT_BRACKET_CHAR, new RegexpOperator(RIGHT_BRACKET_CHAR, count++){

			@Override
			protected boolean needJoinBetween(RegexpElement otherElem) {
				// TODO Auto-generated method stub
				if(otherElem.isOperand())
					return true;
				if(otherElem instanceof RegexpOperator){
					return ((RegexpOperator) otherElem).isLeftBracket();
				}
				return false;
			}
			
		});
		regexpOperators.put(END_OF_REGULAR_CHAR, endOperator = new RegexpOperator(END_OF_REGULAR_CHAR, count++));
		regexpOperators.put(OR_REGULAR_CHAR, orOperator = new RegexpOperator(OR_REGULAR_CHAR, count++));
		regexpOperators.put(JOIN_OF_REGULAR_CHAR, joinOperator = new RegexpOperator(JOIN_OF_REGULAR_CHAR, count++));
		regexpOperators.put(CLOSUER_REGULAR_CHAR, new RegexpOperator(CLOSUER_REGULAR_CHAR, count++){
			
			@Override
			protected boolean needJoinBetween(RegexpElement otherElem) {
				// TODO Auto-generated method stub
				if(otherElem.isOperand())
					return true;
				if(otherElem instanceof RegexpOperator){
					return ((RegexpOperator) otherElem).isLeftBracket();
				}
				return false;
			}
			
		});
		regexpOperators.put(ONCE_OR_NONE_CHAR, new RegexpOperator(ONCE_OR_NONE_CHAR, count++));
		
		precedeArray = new int[count][count];
		for(int r = 0; r < count; ++r){
			for(int c = 0; c < count; ++c)
				precedeArray[r][c] = UNSUPPORTED_PRECEDE;
		}
		
		// left bracket expression
		precedeArray[0][0] = -0;
		precedeArray[0][1] = 0;
		for(int i = 2; i < count; ++i)
			precedeArray[0][i] = -1;
		
		//right bracket expression
//		precedeArray[1][0] = UNSUPPORTED_PRECEDE;
		for(int i = 1; i < count; ++i)
			precedeArray[1][i] = 1;
		
		// end of regular expression
		precedeArray[2][0] = -1;
//		precedeArray[2][1] = UNSUPPORTED_PRECEDE;
		precedeArray[2][2] = 0;
		for(int i = 3; i < count; ++i)
			precedeArray[2][i] = -1;
		
		// or regular expression
		precedeArray[3][0] = -1;
		precedeArray[3][1] = 1;
		precedeArray[3][2] = 1;
		precedeArray[3][3] = 1;
		precedeArray[3][4] = -1;
		precedeArray[3][5] = -1;
		precedeArray[3][6] = -1;
		
		// join regular expression
		precedeArray[4][0] = -1;
		precedeArray[4][1] = 1;
		precedeArray[4][2] = 1;
		precedeArray[4][3] = 1;
		precedeArray[4][4] = 1;
		precedeArray[4][5] = -1;
		precedeArray[4][6] = -1;
		
		// closure regular expression
		precedeArray[5][0] = -1;
		precedeArray[5][1] = 1;
		precedeArray[5][2] = 1;
		precedeArray[5][3] = 1;
		precedeArray[5][4] = 1;
		precedeArray[5][5] = 1;
		precedeArray[5][6] = 1;
		
		// once or none expression
		precedeArray[6][0] = -1;
		precedeArray[6][1] = 1;
		precedeArray[6][2] = 1;
		precedeArray[6][3] = 1;
		precedeArray[6][4] = 1;
		precedeArray[6][5] = 1;
		precedeArray[6][6] = 1;
	}
	
	private RegexpOperator(char operator, int id){
		super();
		this.operator = operator;
		this.id = id;
	}
	
	protected static RegexpOperator getInstance(char operator){
		return regexpOperators.get(operator);
	}
	
	protected static RegexpOperator getOrOperator(){
		return orOperator;
	}
	
	protected static RegexpOperator getJoinOperator(){
		return joinOperator;
	}
	
	protected static RegexpOperator getEndOperator(){
		return endOperator;
	}

	@Override
	protected boolean isOperator() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	protected boolean isOperand() {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	protected CharRange getCharRange() {
		// TODO Auto-generated method stub
		return new CharRange(this.operator);
	}

	@Override
	protected String toRegexString() {
		// TODO Auto-generated method stub
		return this.operator + "";
	}

	@Override
	protected int getPrecede(RegexpElement otherElem) throws IllegalArgumentException {
		// TODO Auto-generated method stub
		if(otherElem == null || otherElem.isOperand())
			throw illegalArgumentException(this, otherElem);
		if(otherElem instanceof RegexpOperator){
			RegexpOperator otherOp = (RegexpOperator) otherElem;
			if(this.checkId() && otherOp.checkId()){
				int result = precedeArray[this.id][otherOp.id];
				if(-1 <= result && 1 >= result)
					return result;
			}
		}
		throw new IllegalArgumentException("--reasons: sorry the precede between \""+
				this.toRegexString() +"\" and \""+otherElem.toRegexString()+"\" is not defined.");
	}
	
	private boolean checkId(){
		return this.id >= 0 && this.id < precedeArray.length;
	}


	@Override
	protected boolean isEndElement() {
		// TODO Auto-generated method stub
		return this.operator == END_OF_REGULAR_CHAR;
	}
	
	@Override
	protected boolean isJoinOperator() {
		// TODO Auto-generated method stub
		return this.operator == JOIN_OF_REGULAR_CHAR;
	}

	@Override
	protected boolean isOrOperator() {
		// TODO Auto-generated method stub
		return this.operator == OR_REGULAR_CHAR;
	}

	@Override
	protected boolean needJoinBetween(RegexpElement otherElem) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + operator;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RegexpOperator other = (RegexpOperator) obj;
		if (operator != other.operator)
			return false;
		return true;
	}

	@Override
	protected boolean isClosureOperator() {
		// TODO Auto-generated method stub
		return this.operator == CLOSUER_REGULAR_CHAR;
	}

	@Override
	protected boolean isOnceOrNoneOperator() {
		// TODO Auto-generated method stub
		return this.operator == ONCE_OR_NONE_CHAR;
	}

	@Override
	protected boolean isLeftBracket() {
		// TODO Auto-generated method stub
		return this.operator == LEFT_BRACKET_CHAR;
	}

	@Override
	protected boolean isRightBracket() {
		// TODO Auto-generated method stub
		return this.operator == RIGHT_BRACKET_CHAR;
	}

}

class RegexpOprand extends RegexpElement {
	private CharRange range;
	
	protected RegexpOprand(CharRange range){
		super();
		this.range = range;
	}
	
	protected RegexpOprand(char ch){
		super();
		this.range = new CharRange(ch);
	}

	@Override
	protected boolean isOperator() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected boolean isOperand() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	protected CharRange getCharRange() {
		// TODO Auto-generated method stub
		return this.range;
	}

	@Override
	protected String toRegexString() {
		// TODO Auto-generated method stub
		return this.getCharRange().toRegexString();
	}

	@Override
	protected boolean isEndElement() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected boolean needJoinBetween(RegexpElement otherElem) {
		// TODO Auto-generated method stub
		if(otherElem.isOperand())
			return true;
		if(otherElem instanceof RegexpOperator){
			return ((RegexpOperator) otherElem).isLeftBracket();
		}
		return false;
	}

	@Override
	protected boolean isJoinOperator() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected boolean isOrOperator() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected boolean isClosureOperator() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected boolean isOnceOrNoneOperator() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected boolean isLeftBracket() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected boolean isRightBracket() {
		// TODO Auto-generated method stub
		return false;
	}
	
}

class RegexpElementFactory {
	protected static RegexpElement getOprand(CharRange range){
		return new RegexpOprand(range);
	}
	
	protected static RegexpElement getOprand(char ch){
		return new RegexpOprand(ch);
	}
	
	protected static RegexpElement getOperator(char operator){
		return RegexpOperator.getInstance(operator);
	}
	
	protected static RegexpElement getOrOperator(){
		return RegexpOperator.getOrOperator();
	}
	
	protected static RegexpElement getJoinOperator(){
		return RegexpOperator.getJoinOperator();
	}
	
	protected static RegexpElement getEndOperator(){
		return RegexpOperator.getEndOperator();
	}
}
