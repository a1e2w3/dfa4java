package hust.idc.automaton;



import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

class TestAutomaton {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// [\d\w]*d*h
		DFABuilder builder = new DFABuilder();
		//Pattern pattern = Pattern.compile("[\\d\\w]?\\p{Digit}\\S\\s[0\\]]{2,}\\x31.\\0062\\0630\\02d*h");
//		Pattern pattern = Pattern.compile("[\\d\\w]?\\p{Digit}[01]{2,}\\x31.\\0062\\0630[\\02|1]d*h");
		Pattern pattern = Pattern.compile("[\\d\\w]\\p{Digit}[01]\\x31.\\0062\\0630[\\02|1]d*h");
		Pattern pattern2 = Pattern.compile("\\p{Alnum}*");
		DFA fa = builder.getInstance(pattern);
		//System.out.println(fa);
		System.out.println(fa.getPattern().toString());
		System.out.println(fa.acceptStringCount());
		System.out.println(fa.minDepth());
		System.out.println(fa.maxDepth());
		System.out.println(fa.acceptStrings(10));
		System.out.println(fa.acceptStrings(10).size());
		fa.clearPattern();
		pattern = fa.getPattern();
		System.out.println(pattern.toString());
		
		DFA fa2 = builder.getInstance(pattern2);
//		System.out.println(fa2);
		//fa2.clearPattern();
		System.out.println(fa2.getPattern());
		System.out.println(fa2.acceptStringCount());
		fa = fa.intersectWith(fa2);
		System.out.println(fa.getPattern());
		System.out.println(fa2.minDepth());
		System.out.println(fa2.maxDepth());
//		System.out.println(fa2.acceptStrings(20));
//		System.out.println(fa2.acceptStrings(20).size());
		
		pattern2 = Pattern.compile("\\p{Alnum}");
		fa2 = builder.getInstance(pattern2);
		fa2.clearPattern();
		System.out.println(fa2.getPattern());
		System.out.println(fa2.acceptStringCount());
		System.out.println(fa2.getExample(0));
		System.out.println(fa2.minDepth());
		System.out.println(fa2.maxDepth());
		System.out.println(fa2.acceptStrings());
		System.out.println(fa2.acceptStrings(10).size());
		
		System.out.println(DFA.FULL_DFA.getPattern());
		System.out.println(DFA.EMPTY_DFA.getPattern());
		
		DFA intersection = DFA.FULL_DFA.intersectWith(DFA.EMPTY_DFA);
		System.out.println(intersection);
		System.out.println(intersection.getPattern());
		
		Map<StateID, DFAState> statusMap = new HashMap<StateID, DFAState>();
		Set<StateID> endStatusIds = new HashSet<StateID>();
		
		DFAState end = DFAState.defaultEndState();
		statusMap.put(end.getStateId(), end);
		endStatusIds.add(end.getStateId());
		DFAState state1 = new DFAState();
		state1.addTransition('a', state1);
		state1.addTransition('b', end);
		statusMap.put(state1.getStateId(), state1);
		
		DFAState state2 = new DFAState();
		endStatusIds.add(state2.getStateId());
		state1.addTransition('c', state2);
		statusMap.put(state2.getStateId(), state2);
		DFA fa1 = new DFA(state1.getStateId(), statusMap, endStatusIds);
		System.out.println(fa1.getPattern());
		
		DFA intersection2 = DFA.FULL_DFA.intersectWith(fa1);
//		System.out.println(intersection2);
		System.out.println(intersection2.getPattern());
		intersection2.dispose();
		
		intersection2 = DFA.EMPTY_DFA.intersectWith(fa1);
//		System.out.println(intersection2);
		System.out.println(intersection2.getPattern());
		
		DFA temp1 = DFA.FULL_DFA.minus(fa1);
//		System.out.println(temp1);
		System.out.println(temp1.getPattern());
//		System.out.println(fa1);
		DFA temp2 = fa1.complement();
//		System.out.println(temp2);
		System.out.println(temp2.getPattern());
		System.out.println(temp1.equals(temp2));
		DFA temp3 = fa1.minus(DFA.EMPTY_DFA).complement();
//		System.out.println(temp3);
		System.out.println(temp3.getPattern());
		System.out.println(temp1.equals(temp3));
		System.out.println(temp2.equals(temp3));
		
		statusMap = new HashMap<StateID, DFAState>();
		endStatusIds = new HashSet<StateID>();
		end = DFAState.defaultEndState();
		statusMap.put(end.getStateId(), end);
		endStatusIds.add(end.getStateId());
		state1 = new DFAState();
		state1.addTransition('b', end);
		statusMap.put(state1.getStateId(), state1);
		
		state2 = new DFAState();
		state2.addTransition('a', state1);
		state1.addTransition('a', state2);
		state2.addTransition('b', end);
		statusMap.put(state2.getStateId(), state2);
		
		DFAState state3 = new DFAState();
		endStatusIds.add(state3.getStateId());
		state1.addTransition('c', state3);
		state2.addTransition('c', state3);
		statusMap.put(state3.getStateId(), state3);
		DFA fa3 = new DFA(state1, statusMap, endStatusIds);
		System.out.println(fa3.getPattern());
		
		fa3.minimize();
		System.out.println(fa3.getPattern());

		DFA fa4 = builder.getInstance(Pattern.compile("[a-t&&[aeiou]]"));
		fa4.clearPattern();
		System.out.println(fa4.getPattern());
		
		DFA fa5 = fa3.unionWith(fa4);
		System.out.println(fa5.getPattern());
		fa3.dispose();
		fa4.dispose();
		fa5.dispose();
	}

}
