package hust.idc.automaton;

public final class CharacterEscaper {
	/**
	 * Character line break in ASCII: '\n'
	 */
	protected static final char LINE_BREAK_CHAR = '\n';
	/**
	 * Character tab in ASCII: '\t'
	 */
	protected static final char TAB_CHAR = '\t';
	/**
	 * Character enter in ASCII: '\r'
	 */
	protected static final char ENTER_CHAR = '\r';
	/**
	 * Form feed character in ASCII: '\f'
	 */
	protected static final char FORM_FEED_CHAR = '\f';
	/**
	 * Character space in ASCII: ' '
	 */
	protected static final char SPACE_CHAR = ' ';
	/**
	 * Escape char: '\'
	 */
	protected static final char ESCAPE_CHAR = '\\';
	
	public static String getEscape(char ch){
		switch(ch){
		case '*':
		case '+':
		case '?':
		case '.':
		case '^':
		case '$':
		case '&':
		case '(':
		case ')':
		case '[':
		case ']':
		case '{':
		case '}':
		case '|':
		case '-':
		case ESCAPE_CHAR: 
			return "" + ESCAPE_CHAR + ch;
		case LINE_BREAK_CHAR:
			return ESCAPE_CHAR + "n";
		case TAB_CHAR:
			return ESCAPE_CHAR + "t";
		case ENTER_CHAR:
			return ESCAPE_CHAR + "r";
		case FORM_FEED_CHAR:
			return ESCAPE_CHAR + "f";
		default:
			return ch + "";
		}
	}
	
	public static CharSequence getEscape(CharSequence sequence, boolean caseIntensive){
		if(sequence == null)
			return null;
		
		StringBuilder builder = new StringBuilder();
		for(int i = 0; i < sequence.length(); ++i){
			char ch = sequence.charAt(i);
			if(!caseIntensive && Character.isLetter(ch)){
				builder.append('[');
				builder.append(Character.toLowerCase(ch));
				builder.append(Character.toUpperCase(ch));
				builder.append(']');
			} else {
				builder.append(getEscape(ch));
			}
		}
		return builder;
	}

}
