package hust.idc.automaton;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Unicode Character Set Presented by a Range
 * @author WangCong
 *
 */
class CharRange implements Serializable, Cloneable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7765173588421279402L;
	private final char start;
	private final char end;
	/**
	 * <strong>true</strong> means character set: [start, end]
	 * <strong>false</strong> means character set: [^start, end]
	 * that is character set of which not in range [start, end]
	 */
	private final boolean inRange; 
	
	/**
	 * Used as epsilon transfer in NFA
	 * [\0, \0]
	 */
	protected static final CharRange EPSILON_RANGE;
	/**
	 * Character set of the whole alphabet, but do not contain '\0'
	 * [0x0001, 0xffff]
	 */
	protected static final CharRange UNIVERSAL_RANGE;
	/**
	 * Character '\n' 
	 * [\n, \n]
	 */
	protected static final CharRange LINE_BREAK_RANGE;
	/**
	 * Digit characters
	 * [0-9]
	 */
	protected static final CharRange DIGIT_RANGE;
	/**
	 * Lower case characters
	 * [a-z]
	 */
	protected static final CharRange LOWER_CASE_RANGE;
	/**
	 * Upper case characters
	 * [A-Z]
	 */
	protected static final CharRange UPPER_CASE_RANGE;
	/**
	 * Printable characters in ASCII table
	 * Contains digit, letters and punctuations
	 * Do not contains character ' ', '\n', '\t', '\r' etc.
	 * [!-~]
	 */
	protected static final CharRange GRAPH_RANGE;
	/**
	 * Character set of the whole ASCII table
	 * [0x0001-0x007f]
	 */
	protected static final CharRange ASCII_RANGE;
	
	/**
	 * Complement of character set ['\n', '\n']
	 * Used for regex "."
	 * [^'\n', '\n']
	 */
	protected static final CharRange NOT_LINE_BREAK_RANGE;
	/**
	 * Complement of digit character set
	 * [^0-9]
	 */
	protected static final CharRange NOT_DIGIT_RANGE;
	/**
	 * Complement of lower case character set
	 * [^a-z]
	 */
	protected static final CharRange NOT_LOWER_CASE_RANGE;
	/**
	 * Complement of upper case character set
	 * [^A-Z]
	 */
	protected static final CharRange NOT_UPPER_CASE_RANGE;
	/**
	 * Complement of printable character set
	 * [^!-~]
	 */
	protected static final CharRange NOT_GRAPH_RANGE;
	/**
	 * Complement of ASCII character set
	 * [0x0080-0xffff] or [^0x0001-0x007f]
	 */
	protected static final CharRange NOT_ASCII_RANGE;
	
	/**
	 * Chinese words set in Unicode table
	 * [0x4E00-0x9fff]
	 */
	protected static final CharRange CHINESE_WORD_RANGE;
	/**
	 * complement of Chinese words set
	 * [^0x4E00-0x9fff]
	 */
	protected static final CharRange NOT_CHINESE_WORD_RANGE;
	
	/**
	 * The minimal value of alphabet: 0x0001
	 */
	protected static final char CHAR_MIN_VALUE = (char) (Character.MIN_VALUE + 1);
	/**
	 * The maximal value of alphabet: 0xffff
	 */
	protected static final char CHAR_MAX_VALUE = Character.MAX_VALUE;
	/**
	 * Character used as EPSILON: '\0'
	 */
	private static final char EPSILON_CHAR = Character.MIN_VALUE;
	
	private static final List<CharRange> PREDEFINED_RANGE_SET;
	
	static {
		EPSILON_RANGE = new CharRange(EPSILON_CHAR);
		LINE_BREAK_RANGE = new CharRange(CharacterEscaper.LINE_BREAK_CHAR){
			/**
			 * 
			 */
			private static final long serialVersionUID = 8499578397516765054L;
			protected final String toRegexString(){
				return getEscape(CharacterEscaper.LINE_BREAK_CHAR);
			}
			protected final CharRange complement(){
				return NOT_LINE_BREAK_RANGE;
			}
		};
		DIGIT_RANGE = new CharRange('0', '9'){
			/**
			 * 
			 */
			private static final long serialVersionUID = 2571337929474570371L;
			protected final String toRegexString(){
				return CharacterEscaper.ESCAPE_CHAR + "d";
			}
			protected final CharRange complement(){
				return NOT_DIGIT_RANGE;
			}
		};
		LOWER_CASE_RANGE = new CharRange('a', 'z'){
			/**
			 * 
			 */
			private static final long serialVersionUID = 2671735525064851380L;
			protected final String toRegexString(){
				return CharacterEscaper.ESCAPE_CHAR + "p{Lower}";
				//return "[a-z]";
			}
			protected final CharRange complement(){
				return NOT_LOWER_CASE_RANGE;
			}
		};
		UPPER_CASE_RANGE = new CharRange('A', 'Z'){
			/**
			 * 
			 */
			private static final long serialVersionUID = -1893288749195685404L;
			protected final String toRegexString(){
				return CharacterEscaper.ESCAPE_CHAR + "p{Upper}";
				//return "[A-Z]";
			}
			protected final CharRange complement(){
				return NOT_UPPER_CASE_RANGE;
			}
		};
		GRAPH_RANGE = new CharRange('!', '~'){
			/**
			 * 
			 */
			private static final long serialVersionUID = 2374257497163389881L;
			protected final String toRegexString(){
				return CharacterEscaper.ESCAPE_CHAR + "p{Graph}";
				//return "[!-~]";
			}
			protected final CharRange complement(){
				return NOT_GRAPH_RANGE;
			}
		};
		ASCII_RANGE = new CharRange(CHAR_MIN_VALUE, (char) 0x007f){
			/**
			 * 
			 */
			private static final long serialVersionUID = 1038250411788560178L;
			protected final String toRegexString(){
				return CharacterEscaper.ESCAPE_CHAR + "p{ASCII}";
				//return "[" + ESCAPE_CHAR + "u0001-" + ESCAPE_CHAR + "u007f]";
			}
			protected final CharRange complement(){
				return NOT_ASCII_RANGE;
			}
		};
		NOT_LINE_BREAK_RANGE = new CharRange(CharacterEscaper.LINE_BREAK_CHAR, false){
			/**
			 * 
			 */
			private static final long serialVersionUID = 3540062972169559050L;
			protected final String toRegexString(){
				return ".";
			}
			protected final CharRange complement(){
				return LINE_BREAK_RANGE;
			}
		};
		NOT_DIGIT_RANGE = new CharRange('0', '9', false){
			/**
			 * 
			 */
			private static final long serialVersionUID = 3571257097341612568L;
			protected final String toRegexString(){
				return CharacterEscaper.ESCAPE_CHAR + "D";
			}
			protected final CharRange complement(){
				return DIGIT_RANGE;
			}
		};
		NOT_LOWER_CASE_RANGE = new CharRange('a', 'z', false){
			/**
			 * 
			 */
			private static final long serialVersionUID = -3984031224072315846L;
			protected final String toRegexString(){
				return "[^" + CharacterEscaper.ESCAPE_CHAR + "p{Lower}]";
				//return "[^a-z]";
			}
			protected final CharRange complement(){
				return LOWER_CASE_RANGE;
			}
		};
		NOT_UPPER_CASE_RANGE = new CharRange('A', 'Z', false){
			/**
			 * 
			 */
			private static final long serialVersionUID = 6273030361437224546L;
			protected final String toRegexString(){
				return "[^" + CharacterEscaper.ESCAPE_CHAR + "p{Upper}]";
				//return "[^A-Z]";
			}
			protected final CharRange complement(){
				return UPPER_CASE_RANGE;
			}
		};
		NOT_GRAPH_RANGE = new CharRange('!', '~', false){
			/**
			 * 
			 */
			private static final long serialVersionUID = -7899629124329008217L;
			protected final String toRegexString(){
				return "[^" + CharacterEscaper.ESCAPE_CHAR + "p{Graph}]";
				//return "[^!-~]";
			}
			protected final CharRange complement(){
				return GRAPH_RANGE;
			}
		};
		NOT_ASCII_RANGE = new CharRange(CHAR_MIN_VALUE, (char) 0x007f, false){
			/**
			 * 
			 */
			private static final long serialVersionUID = 3428071275972235394L;
			protected final String toRegexString(){
				return "[^" + CharacterEscaper.ESCAPE_CHAR + "p{ASCII}]";
				//return "[^" + ESCAPE_CHAR + "u0001-" + ESCAPE_CHAR + "u007f]";
			}
			protected final CharRange complement(){
				return ASCII_RANGE;
			}
		};
		CHINESE_WORD_RANGE = new CharRange((char) 0x4E00, (char) 0x9FFF){
			/**
			 * 
			 */
			private static final long serialVersionUID = 7676968106940056954L;

			protected final CharRange complement(){
				return NOT_CHINESE_WORD_RANGE;
			}
		};
		NOT_CHINESE_WORD_RANGE = new CharRange((char) 0x4E00, (char) 0x9FFF, false){
			/**
			 * 
			 */
			private static final long serialVersionUID = 3754924993783279034L;

			protected final CharRange complement(){
				return CHINESE_WORD_RANGE;
			}
		};
		UNIVERSAL_RANGE = new CharRange(CHAR_MIN_VALUE, CHAR_MAX_VALUE){
			/**
			 * 
			 */
			private static final long serialVersionUID = 8860518886881141688L;
			protected final String toRegexString(){
				return "[" + DIGIT_RANGE.toRegexString() + 
					NOT_DIGIT_RANGE.toRegexString() +"]";
			}
			protected final CharRange complement(){
				return null;
			}
		};
		
		PREDEFINED_RANGE_SET = new ArrayList<CharRange>();
		PREDEFINED_RANGE_SET.add(UNIVERSAL_RANGE);
		PREDEFINED_RANGE_SET.add(LINE_BREAK_RANGE);
		PREDEFINED_RANGE_SET.add(DIGIT_RANGE);
		PREDEFINED_RANGE_SET.add(LOWER_CASE_RANGE);
		PREDEFINED_RANGE_SET.add(UPPER_CASE_RANGE);
		PREDEFINED_RANGE_SET.add(GRAPH_RANGE);
		PREDEFINED_RANGE_SET.add(ASCII_RANGE);
		PREDEFINED_RANGE_SET.add(NOT_LINE_BREAK_RANGE);
		PREDEFINED_RANGE_SET.add(NOT_DIGIT_RANGE);
		PREDEFINED_RANGE_SET.add(NOT_LOWER_CASE_RANGE);
		PREDEFINED_RANGE_SET.add(NOT_UPPER_CASE_RANGE);
		PREDEFINED_RANGE_SET.add(NOT_GRAPH_RANGE);
		PREDEFINED_RANGE_SET.add(NOT_ASCII_RANGE);
	}
	/**
	 * Constructor of set [start, end] or [^start, end]
	 * @param start 
	 * 			The start point of the range
	 * @param end 
	 * 			The end point of the range
	 * @param inRange 
	 * 			true means range [start, end], false means range [^start, end] 
	 * 			that is character set of which not in range [start, end]
	 * @throws IllegalArgumentException 
	 * 			if the start point is greater than the end point than throw IlegalArgumentException
	 */
	protected CharRange(char start, char end, boolean inRange)
		throws IllegalArgumentException{
		if(start > end)
			throw new IllegalArgumentException("start character must not be bigger than end character");
		this.start = start;
		this.end = end;
		this.inRange = inRange;
	}
	
	/**
	 * Constructor of set [start, end]
	 * @param start 
	 * 			The start point of the range
	 * @param end 
	 * 			The end point of the range
	 * @throws IllegalArgumentException 
	 * 			If the start point is greater than the end point than throw IlegalArgumentException
	 */
	protected CharRange(char start, char end) 
		throws IllegalArgumentException{
		this(start, end, true);
	}
	
	/**
	 * Constructor of set [ch, ch] which only contains one character ch
	 * @param character 
	 * 			character which is contained by range
	 */
	protected CharRange(char character){
		this(character, character, true);
	}
	
	/**
	 * Constructor of set [ch, ch] which only contains one character ch 
	 * or [^ch, ch] which contains all characters except ch
	 * @param character 
	 * 			character which is contained or the only one not contained by the set
	 * @param inRange 
	 * 			true means set [character, character], false set [^character, character]
	 */
	protected CharRange(char character, boolean inRange){
		this(character, character, inRange);
	}
	
	/**
	 * Constructor of range of the whole alphabet [0x0001, 0xffff]
	 */
	protected CharRange(){
		this(CHAR_MIN_VALUE, CHAR_MAX_VALUE, true);
	}
	
	/**
	 * Get the start point of the range
	 * @return 
	 * 		The start point
	 */
	protected final char getStart() {
		return start;
	}

	/**
	 * Get the end point of the range
	 * @return 
	 * 		The end point
	 */
	protected final char getEnd() {
		return end;
	}

	/**
	 * Check whether the set needs to be negated
	 * @return 
	 * 		return true if the range means set: [start, end]
	 * 		return false if the range means set: [^start, end]
	 */
	protected final boolean isInRange() {
		return inRange;
	}

	/**
	 * Check whether a character is contained by the set
	 * @param ch 
	 * 		The character needs to be checked
	 * @return 
	 * 		return true if the the set contains the character
	 * 		else return false
	 */
	protected final boolean charIn(char ch){
		return (ch >= this.start && ch <= this.end) ^ !this.inRange;
	}
	
	/**
	 * Get the complement set of the set
	 * @return  The complement set of the set
	 */
	protected CharRange complement(){
		int index = PREDEFINED_RANGE_SET.indexOf(this);
		if(index >= 0)
			return PREDEFINED_RANGE_SET.get(index).complement();
		if(this.equals(CHINESE_WORD_RANGE))
			return NOT_CHINESE_WORD_RANGE;
		if(this.equals(NOT_CHINESE_WORD_RANGE))
			return CHINESE_WORD_RANGE;
			
		return new CharRange(this.getStart(), this.getEnd(), !this.isInRange());
	}
	
	/**
	 * Get the intersection of two sets
	 * @param anotherRange another character set presented as a range
	 * @return The intersection of two sets
	 * @throws Throwable 
	 */
	protected final List<CharRange> intersectWith(CharRange anotherRange) {
		if(anotherRange == null)
			return new ArrayList<CharRange>();
		return CharRangeOperation.intersect(this, anotherRange);
	}
	
	protected final List<CharRange> intersectWith(Collection<? extends CharRange> unionList) {
		return CharRangeOperation.intersect(this, unionList);
	}
	
	protected final List<CharRange> minus(CharRange subtractor) {
		return CharRangeOperation.minus(this, subtractor);
	}
	
	/**
	 * Get the character set's size, that is the amount of characters contained by the set
	 * @return 
	 */
	protected final int size(){
		int size = this.end - this.start + 1;
		return this.inRange ? size : CHAR_TOTAL_NUMBER - size;
	}

	/**
	 * Get the regex of the set
	 * @return The regex of the set
	 */
	protected String toRegexString(){
		int index = PREDEFINED_RANGE_SET.indexOf(this);
		if(index >= 0)
			return PREDEFINED_RANGE_SET.get(index).toRegexString();
		String notCh = this.inRange ? "" : "^";
		String regex = notCh + getEscape(this.start);
		if(this.end > this.start){
			if(this.end - this.start > 1)
				regex += "-";
			regex += getEscape(this.end);
		} 
		if(this.end - this.start >= 1)
			return "[" + regex + "]";
		else
			return regex;
	}
	
	protected char getExample(){
		if(this.inRange)
			return (char) ((this.start + this.end) / 2);
		else if(this.start > CHAR_MIN_VALUE)
			return (char) ((this.start + CHAR_MIN_VALUE - 1) / 2);
		else if(this.end < CHAR_MAX_VALUE)
			return (char) ((this.end + CHAR_MAX_VALUE + 1) / 2);
		else
			return EPSILON_CHAR;
	}
	
	protected Set<Character> getCharSet(){
		Set<Character> set = new HashSet<Character>();
		if(this.inRange){
			for(int i = this.start; i <= this.end; ++i){
				set.add(new Character((char) i));
			}
		} else {
			for(int i = CHAR_MIN_VALUE; i < this.start; ++i){
				set.add(new Character((char) i));
			}
			for(int i = CHAR_MAX_VALUE; i > this.end; --i){
				set.add(new Character((char) i));
			}
		}
		return set;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString(java.lang.Object)
	 */
	@Override
	public String toString(){
		String str = this.inRange ? "" : "^";
		if(this.start == this.end)
			return str + getEscape(this.start);
		else
			return str + getEscape(this.start) + "-" + getEscape(this.end);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj){
		if(this == obj)
			return true;
		if(obj == null)
			return false;
		if (!(obj instanceof CharRange)) {
			return false;
		}
		
		final CharRange cr = (CharRange) obj;
		if(this.size() != cr.size())
			return false;
		if(this.inRange != cr.inRange){
			if(cr.start == CHAR_MIN_VALUE && 
					this.end == CHAR_MAX_VALUE && 
					this.start - 1 == (int) cr.end)
				return true;
			else if(this.start == CHAR_MIN_VALUE && 
					cr.end == CHAR_MAX_VALUE && 
					this.end + 1 == (int) cr.start)
				return true;
			else
				return false;
		} else {
			return this.start == cr.start && 
				this.end == cr.end;
		}
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode(){
		if(!this.inRange){
			if(this.start == CHAR_MIN_VALUE){
				return caculateHash(this.end + 1, CHAR_MAX_VALUE);
			}
			else if(this.end == CHAR_MAX_VALUE){
				return caculateHash(CHAR_MIN_VALUE, this.start - 1);
			}
		}
		return caculateHash(this.start, this.end);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	protected CharRange clone() {
		// TODO Auto-generated method stub
		try{
			return (CharRange) super.clone();
		} catch(CloneNotSupportedException e){
			System.out.println(e.toString());
			return null;
		} catch(Exception e){
			System.out.println(e.toString());
			return null;
		}
	}
	
	private static final int caculateHash(int value1, int value2) {
		final int prime = 31;
		int result = 1;
		result = prime * result + value1;
		result = prime * result + value2;
		return result;
	}
	
	private static final String getEscape(char ch){
		if(printable(ch))
			return CharacterEscaper.getEscape(ch);
		else
			return CharacterEscaper.ESCAPE_CHAR + "u" + getUnicode(ch);
	}
	
	private static final String getUnicode(char ch){
		return String.format("%04x", (int) ch);
	}
	
	private static final boolean printable(char ch){
		switch(ch){
		case CharacterEscaper.LINE_BREAK_CHAR: 
		case CharacterEscaper.ENTER_CHAR:
		case CharacterEscaper.ESCAPE_CHAR: 
		case CharacterEscaper.FORM_FEED_CHAR:
		case CharacterEscaper.SPACE_CHAR:
		case CharacterEscaper.TAB_CHAR: return true;
		default: return GRAPH_RANGE.charIn(ch);
		}
	}
	
	private static final int CHAR_TOTAL_NUMBER = Character.MAX_VALUE - Character.MIN_VALUE - 1;
	
}
