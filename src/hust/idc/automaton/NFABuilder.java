package hust.idc.automaton;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.regex.Pattern;

class NFABuilder {
	private Map<StateID, NFAState> statesMap;
	
	protected NFABuilder(){
	}
	
	private void init(){
		this.statesMap = new HashMap<StateID, NFAState>();
	}
	
	private void cleanup(){
		// cannot clear the map
		this.statesMap = null;
	}
	
	protected NFA getInstance(DFA dfa){
		this.init();
		Iterator<DFAState> stateIt = dfa.getStateIterator();
		while(stateIt.hasNext()){
			NFAState state = new NFAState(stateIt.next());
			this.statesMap.put(state.getStateId(), state);
		}
		Set<StateID> endStateIds = new HashSet<StateID>(dfa.getEndStateIds());
		StateID startStateId = dfa.getStartStateId();
		NFA nfa = new NFA(startStateId, this.statesMap, endStateIds);
		this.cleanup();
		return nfa;
	}
	
	protected NFA getInstance(Pattern pattern){
		RegularExpression regex = RegexParser.getInstance(pattern);
		if(regex.isEmpty()){
			return new NFA();
		}
		this.init();
		
		Stack<TempNFA> oprandStack = new Stack<TempNFA>();
		Stack<RegexpElement> operatorStack = new Stack<RegexpElement>();
		operatorStack.push(RegexpElementFactory.getEndOperator());
		List<RegexpElement> source = regex.getRegularExpression();
		int index = 0;
		while(index < source.size() 
				&&(!(source.get(index).isEndElement())
				|| !(operatorStack.peek().isEndElement()))) {
			if(source.get(index).isOperand()){
				oprandStack.push(baseCreate(source.get(index).getCharRange()));
				++index;
				continue;
			}
		    //����������ַ�֮������ȼ���ϵ
			switch(operatorStack.peek().getPrecede(source.get(index))){
			case -1:
				operatorStack.push(source.get(index));
				++index;
				break;
			case 0:
				operatorStack.pop();
				++index;
				break;
			case 1:
				if(operatorStack.peek().isClosureOperator()){
					TempNFA target = oprandStack.peek();
					operatorStack.pop();
					target.closure();
				}
				if(operatorStack.peek().isOnceOrNoneOperator()){
					TempNFA target = oprandStack.peek();
					operatorStack.pop();
					target.onceOrNone();
				}
				if(operatorStack.peek().isOrOperator()){
					TempNFA b = oprandStack.pop();
					TempNFA a = oprandStack.peek();
					operatorStack.pop();
					a.union(b);
					b.dispose();
				}
				if(operatorStack.peek().isJoinOperator()){
					TempNFA b = oprandStack.pop();
					TempNFA a = oprandStack.peek();
					operatorStack.pop();
					a.join(b);
					b.dispose();
				}
				break;
			default:
				break;
			}
		}

		NFA nfa = oprandStack.pop().toNFA();
		oprandStack.clear();
		operatorStack.clear();
		this.cleanup();
		return nfa;
	}
	
	private TempNFA baseCreate(CharRange c){
		NFAState start = new NFAState();
		NFAState end = new NFAState();
		this.putState(start);
		this.putState(end);
		start.addTransition(c, end.getStateId());
		return new TempNFA(start.getStateId(), end.getStateId());
	}
	
	private final NFAState getState(StateID stateId){
		return this.statesMap.get(stateId);
	}
	
	private final void putState(NFAState state){
		this.statesMap.put(state.getStateId(), state);
	}
	
	private class TempNFA{
		private StateID startId;
		private StateID endId;
		
		private TempNFA(StateID start, StateID end){
			this.startId = start;
			this.endId = end;
		}
		
		private void dispose(){
			this.startId = null;
			this.endId = null;
		}
		
		private NFAState getStartState(){
			return NFABuilder.this.getState(startId);
		}
		
		private NFAState getEndState(){
			return NFABuilder.this.getState(endId);
		}
		
		private NFA toNFA(){
			Set<StateID> endStateIds = new HashSet<StateID>(1);
			endStateIds.add(endId);
			return new NFA(this.startId, NFABuilder.this.statesMap, endStateIds);
		}
		
		private void union(TempNFA nfa){
			NFAState newStart = new NFAState();
			newStart.addNullTransition(this.startId);
			newStart.addNullTransition(nfa.startId);
			NFABuilder.this.putState(newStart);
			
			NFAState newEnd = new NFAState();
			this.getEndState().addNullTransition(newEnd.getStateId());
			nfa.getEndState().addNullTransition(newEnd.getStateId());
			NFABuilder.this.putState(newEnd);
			
			this.startId = newStart.getStateId();
			this.endId = newEnd.getStateId();
		}
		
		private void join(TempNFA nfa){
			NFAState endState = this.getEndState();
			endState.addNullTransition(nfa.startId);
			this.endId = nfa.endId;
		}
		
		private void closure(){
			NFAState endState = this.getEndState();
			endState.addNullTransition(this.startId);
		}
		
		private void onceOrNone(){
			NFAState start = this.getStartState();
			start.addNullTransition(this.endId);
		}
	}

}
