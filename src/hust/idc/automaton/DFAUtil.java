package hust.idc.automaton;

import hust.idc.util.Pair;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Stack;

/**
 * DFA有关操作
 * 
 * @author WangCong
 * 
 */
final class DFAUtil {

	/**
	 * 求两个DFA的交�? *
	 * 
	 * @param dfa1
	 *            DFA1
	 * @param dfa2
	 *            DFA2
	 * @return 两个DFA交集的最小化DFA
	 * @throws Throwable
	 */
	protected static DFA intersect(final DFA dfa1, final DFA dfa2) {
		if(dfa1 == null || dfa1.isEmpty() || dfa2 == null || dfa2.isEmpty())
			return DFA.EMPTY_DFA;
		DFAIntersector intersector = new DFAIntersector(dfa1, dfa2);
		return intersector.intersect();
	}
	
	protected static DFA union(final DFA dfa1, final DFA dfa2) {
		if(dfa1 == null || dfa1.isEmpty()){
			if(dfa2 == null || dfa2.isEmpty())
				return DFA.EMPTY_DFA;
			else
				return dfa2;
		} else if(dfa2 == null || dfa2.isEmpty()){
			return dfa1;
		}
		
		NFABuilder builfer = new NFABuilder();
		NFA nfa = builfer.getInstance(dfa1);
		nfa.union(dfa2);
		DFA result = nfa.determinize();
		nfa.dispose();
		return result;
	}

	/**
	 * 对一个DFA取反
	 * 
	 * @param automaton
	 *            要取反的DFA
	 * @return 取反并最小化后的DFA
	 * @throws Throwable
	 */
	protected static DFA getComplement(final DFA automaton) {
		DFAState start = null;
		Map<StateID, DFAState> statesMap = new HashMap<StateID, DFAState>();
		Set<StateID> endStateIds = new HashSet<StateID>();
		Map<StateID, StateID> stateIdMap = new HashMap<StateID, StateID>();
		
		Iterator<DFAState> statesIt = automaton.getStateIterator();
		while(statesIt.hasNext()){
			DFAState state = statesIt.next();
			DFAState newState = new DFAState();
			statesMap.put(newState.getStateId(), newState);
			stateIdMap.put(state.getStateId(), newState.getStateId());
			
			if (!automaton.isEndState(state.getStateId()))
				endStateIds.add(newState.getStateId());
			
			if (automaton.isStartState(state.getStateId()))
				start = newState;
		}

		DFAState defaultEndState = DFAState.defaultEndState();
		statesMap.put(defaultEndState.getStateId(), defaultEndState);
		stateIdMap.put(DFAState.FAILED_STATE_ID, defaultEndState.getStateId());
		endStateIds.add(defaultEndState.getStateId());

		statesIt = automaton.getStateIterator();
		while (statesIt.hasNext()) {
			DFAState state = statesIt.next();
			DFAState newState = statesMap.get(stateIdMap.get(state.getStateId()));
			
			List<CharRange> failedList = state.getFailedCharRangeList();
			for (int i = 0; i < failedList.size(); ++i) {
				newState.addTransition(failedList.get(i),
						defaultEndState.getStateId());
			}
			
			Iterator<Entry<CharRange, StateID>> transIt = state.getTransitionIterator();
			while(transIt.hasNext()){
				Entry<CharRange, StateID> transEntry = transIt.next();
				newState.addTransition(transEntry.getKey(), stateIdMap.get(transEntry.getValue()));
			}
		}

		stateIdMap.clear();
		DFA complement = new DFA(start, statesMap, endStateIds);
		complement.minimize();
//		System.out.println("Complement: " + complement.getPattern());
		return complement;
	}

	/**
	 * ��һ�������Զ���1��ȥ�����Զ���2�Ĳ(
	 * �����Զ���1�������Զ���2�Ĳ����Ľ���)
	 * 
	 * @param automaton1
	 *            �����Զ���1
	 * @param automaton2
	 *            �����Զ���2
	 * @return �����Զ���1��ȥ�����Զ���2�Ĳ
	 * @throws Throwable
	 */
	protected static DFA minus(final DFA automaton1, final DFA automaton2) {
		return intersect(automaton1, getComplement(automaton2));
	}

	/**
	 * �ж�������С���������Զ����Ƿ�ȼ�?���ܵ��ַ����Ƿ����?
	 * 
	 * @param automaton1
	 *            �����Զ���1
	 * @param automaton2
	 *            �����Զ���2
	 * @return �ȼ۷���true�����򷵻�false
	 */
	protected static boolean equals(final DFA automaton1, final DFA automaton2) {
		if (automaton1 == null || automaton2 == null)
			return false;
		if (automaton1.stateCount() != automaton2.stateCount())
			return false;
		if (automaton1.endStateCount() != automaton2.endStateCount())
			return false;

		// ��ʼ��״̬��Ӧӳ��
		Map<StateID, StateID> stateEqualsMap = new HashMap<StateID, StateID>();
		Iterator<DFAState> auto1It = automaton1.getStateIterator();
		while (auto1It.hasNext()) {
			stateEqualsMap.put(auto1It.next().getStateId(),
					DFAState.FAILED_STATE_ID);
		}
		stateEqualsMap.put(automaton1.getStartStateId(),
				automaton2.getStartStateId());
		stateEqualsMap.put(DFAState.FAILED_STATE_ID, DFAState.FAILED_STATE_ID);

		Stack<Pair<StateID, StateID>> stack = new Stack<Pair<StateID, StateID>>();
		stack.push(new Pair<StateID, StateID>(automaton1.getStartStateId(),
				automaton2.getStartStateId()));
		// ����״̬��һһ��Ӧ��ϵ
		while (!stack.empty()) {
			Pair<StateID, StateID> equalsPair = stack.pop();
			DFAState status1 = automaton1.getState(equalsPair.getFirst());
			DFAState status2 = automaton2.getState(equalsPair.getSecond());
			if (status1 == null || status2 == null)
				return false;

			Set<Character> alphabet = status1.getStartPoints();
			alphabet.addAll(status2.getStartPoints());
			for (char ch : alphabet) {
				StateID trans1 = status1.getTransition(ch);
				StateID trans2 = status2.getTransition(ch);

				if (stateEqualsMap.get(trans1).equals(trans2))
					continue;
				else if (!stateEqualsMap.get(trans1).equals(
						DFAState.FAILED_STATE_ID))
					return false;
				else if (stateEqualsMap.containsValue(trans2))
					return false;
				else if (automaton1.isEndState(trans1) != automaton2
						.isEndState(trans2))
					return false;
				else {
					stateEqualsMap.put(trans1, trans2);
					stack.push(new Pair<StateID, StateID>(trans1, trans2));
				}
			}
		}
		stateEqualsMap.remove(DFAState.FAILED_STATE_ID);

		return !stateEqualsMap.containsValue(DFAState.FAILED_STATE_ID);
	}

	protected static long acceptStringCount(final DFA automaton) {
		if (automaton == null || automaton.isEmpty())
			return 0L;
		else
			return acceptStringCount(automaton, automaton.getStartStateId(),
					new Stack<StateID>(), new HashMap<StateID, Long>());
	}

	private static long acceptStringCount(final DFA automaton,
			final StateID startId, Stack<StateID> path, Map<StateID, Long> cache) {
		if (!automaton.containsState(startId))
			return 0L;
		if (path.contains(startId))
			return -1L;

		path.push(startId);
		long count = automaton.isEndState(startId) ? 1L : 0L;
		DFAState start = automaton.getState(startId);
		for (Iterator<Entry<CharRange, StateID>> transIt = start
				.getTransitionIterator(); transIt.hasNext();) {
			Entry<CharRange, StateID> entry = transIt.next();
			StateID transId = entry.getValue();
			if (transId.equals(startId))
				return -1L;
			if (cache.containsKey(transId)) {
				count += (cache.get(transId) * entry.getKey().size());
			} else {
				long temp = acceptStringCount(automaton, transId, path, cache);
				if (temp < 0)
					return -1L;
				cache.put(transId, temp);
				count += (cache.get(transId) * entry.getKey().size());
			}
		}
		path.pop();
		return count;
	}

	protected static Set<String> acceptStrings(final DFA automaton, int maxSize) {
		if (automaton == null || automaton.isEmpty())
			return new HashSet<String>();
		else
			return acceptStrings(automaton, automaton.getStartStateId(),
					maxSize, new Stack<StateID>(),
					new HashMap<StateID, Set<String>>());
	}

	private static Set<String> acceptStrings(final DFA automaton,
			final StateID startId, int maxSize, Stack<StateID> path,
			HashMap<StateID, Set<String>> cache) {
		// TODO Auto-generated method stub
		if (!automaton.containsState(startId))
			return new HashSet<String>();
		if (path.contains(startId)) {
			return new HashSet<String>();
		}

		if (maxSize == 0) {
			return new HashSet<String>();
		}

		Set<String> result = new HashSet<String>();
		if (automaton.isEndState(startId)) {
			result.add("");
		}
		Set<Character> transToSelf = null;
		path.push(startId);
		DFAState start = automaton.getState(startId);
		for (Iterator<Entry<CharRange, StateID>> transIt = start
				.getTransitionIterator(); transIt.hasNext();) {
			Entry<CharRange, StateID> entry = transIt.next();
			Set<Character> chSet = entry.getKey().getCharSet();
			if (chSet == null || chSet.isEmpty())
				continue;

			StateID transId = entry.getValue();
			if (transId.equals(startId)) {
				transToSelf = chSet;
				continue;
			}

			Set<String> postfix = new HashSet<String>();
			if (cache.containsKey(transId)) {
				postfix = cache.get(transId);
			} else {
				int newSize = maxSize < 0 ? -1 : (maxSize - result.size()
						+ chSet.size() - 1)
						/ chSet.size();
				postfix = acceptStrings(automaton, transId, newSize, path,
						cache);
				cache.put(transId, postfix);
			}
			if (postfix == null)
				continue;
			for (String str : postfix) {
				for (Character ch : chSet) {
					result.add(ch + str);
					if (maxSize >= 0 && result.size() >= maxSize) {
						path.pop();
						return result;
					}
				}
			}
		}
		path.pop();

		if (transToSelf != null && maxSize > 0) {
			while (result.size() < maxSize) {
				for (Character ch : transToSelf) {
					Set<String> temp = new HashSet<String>(result.size());
					temp.addAll(result);
					for (String str : temp) {
						result.add(ch + str);
						if (result.size() >= maxSize)
							return result;
					}
				}
			}
		}

		return result;
	}

	protected static String getExample(final DFA automaton, int length) {
		if (automaton == null || automaton.isEmpty())
			return null;
		else
			return getExample(automaton, automaton.getStartStateId(), length,
					new Stack<StateID>());
	}

	private static String getExample(final DFA automaton,
			final StateID startId, int length, Stack<StateID> path) {
		if (!automaton.containsState(startId))
			return null;
		if (path.contains(startId))
			return null;

		if (length < 0 && automaton.isEndState(startId)) {
			return "";
		}
		if (length == 0)
			return automaton.isEndState(startId) ? "" : null;

		String pathStr = null;
		path.push(startId);
		DFAState start = automaton.getState(startId);
		for (Iterator<Entry<CharRange, StateID>> transIt = start
				.getTransitionIterator(); transIt.hasNext();) {
			Entry<CharRange, StateID> entry = transIt.next();
			StateID transId = entry.getValue();
			if (transId.equals(startId))
				continue;
			else {
				int newLength = length < 0 ? length : length - 1;
				String temp = getExample(automaton, transId, newLength, path);
				if (temp == null)
					continue;
				pathStr = entry.getKey().getExample() + temp;
			}
		}
		path.pop();
		return pathStr;
	}

	protected static int minDepth(final DFA automaton) {
		if (automaton == null || automaton.isEmpty())
			return -1;
		else
			return minDepth(automaton, automaton.getStartStateId(),
					new Stack<StateID>(), new HashMap<StateID, Integer>());
	}

	private static int minDepth(final DFA automaton, StateID startId,
			Stack<StateID> path, Map<StateID, Integer> cache) {
		// TODO Auto-generated method stub
		if (!automaton.containsState(startId))
			return Integer.MAX_VALUE;
		if (path.contains(startId))
			return Integer.MAX_VALUE;

		if (automaton.isEndState(startId))
			return 0;

		int minDepth = Integer.MAX_VALUE;
		path.push(startId);
		DFAState start = automaton.getState(startId);
		for (Iterator<Entry<CharRange, StateID>> transIt = start
				.getTransitionIterator(); transIt.hasNext();) {
			Entry<CharRange, StateID> entry = transIt.next();
			StateID transId = entry.getValue();
			if (transId.equals(startId))
				continue;
			else if (cache.containsKey(transId)) {
				minDepth = Math.min(minDepth, cache.get(transId) + 1);
			} else {
				minDepth = Math.min(minDepth,
						minDepth(automaton, transId, path, cache) + 1);
			}
		}
		path.pop();
		return minDepth;
	}

	protected static int maxDepth(final DFA automaton) {
		if (automaton == null || automaton.isEmpty())
			return -1;
		else
			return maxDepth(automaton, automaton.getStartStateId(),
					new Stack<StateID>(), new HashMap<StateID, Integer>());
	}

	private static int maxDepth(final DFA automaton, StateID startId,
			Stack<StateID> path, Map<StateID, Integer> cache) {
		// TODO Auto-generated method stub
		if (!automaton.containsState(startId))
			return 0;
		if (path.contains(startId))
			return Integer.MAX_VALUE;

		int maxDepth = 0;
		path.push(startId);
		DFAState start = automaton.getState(startId);
		for (Iterator<Entry<CharRange, StateID>> transIt = start
				.getTransitionIterator(); transIt.hasNext();) {
			Entry<CharRange, StateID> entry = transIt.next();
			StateID transId = entry.getValue();
			if (transId.equals(startId))
				return Integer.MAX_VALUE;
			else if (cache.containsKey(transId)) {
				maxDepth = Math.max(maxDepth, cache.get(transId) + 1);
			} else {
				int temp = maxDepth(automaton, transId, path, cache);
				if (temp == Integer.MAX_VALUE) {
					return Integer.MAX_VALUE;
				}
				cache.put(transId, temp);
				maxDepth = Math.max(maxDepth, temp + 1);
			}
		}
		path.pop();
		return maxDepth;
	}

}
