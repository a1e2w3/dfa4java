package hust.idc.automaton;

import hust.idc.util.Pair;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;


/**
 * DFA交集计算的算法
 * @author WangCong
 *
 */
final class DFAIntersector {
	private final class ConnectionState{
		private final Pair<StateID, StateID> location;
		private final DFAState state;
		
		private ConnectionState(StateID xId, StateID yId){
			this.location = new Pair<StateID, StateID>(xId, yId);
			this.state = new DFAState();
		}
		
		private ConnectionState(StateID xId, StateID yId, final DFAState status){
			this.location = new Pair<StateID, StateID>(xId, yId);
			this.state = status;
		}
		
		private final void addTransition(final CharRange charRange, StateID statusId) {
			this.state.addTransition(charRange, statusId);
		}
		
		private final StateID getStateId(){
			return this.state.getStateId();
		}
		
		@Override
		public String toString(){
			return "Status Connection: " + this.location.toString()
				+ ", State Id : " + this.getStateId();
		}
	}
	
	private final DFA fa1;
	private final DFA fa2;
	
	private Map<StateID, ConnectionState> connectionMap;
	private DFAState startState;
	private Map<Pair<StateID, StateID>, StateID> connectionMatrix;
	private Map<StateID, DFAState> statesMap;
	private Set<StateID> endStateIds;
	
	@SuppressWarnings("unused")
	private DFAIntersector(){
		super();
		this.fa1 = null;
		this.fa2 = null;
	}
	
	/**
	 * 构造方法
	 * @param fa1 DFA1
	 * @param fa2 DFA2
	 */
	protected DFAIntersector(final DFA fa1, final DFA fa2){
		super();
		this.fa1 = fa1;
		this.fa2 = fa2;
	}
	
	private final void init(){
		this.connectionMap = new HashMap<StateID, ConnectionState>();
		this.statesMap = new HashMap<StateID, DFAState>();
		this.endStateIds = new HashSet<StateID>();
		this.connectionMatrix = new HashMap<Pair<StateID, StateID>, StateID>();
	}
	

	private void cleanup(){
		// cannot clear the map and set
		this.startState = null;
		this.statesMap = null;
		this.endStateIds = null;
		this.connectionMap.clear();
		this.connectionMap = null;
		this.connectionMatrix.clear();
		this.connectionMatrix = null;
	}

	/**
	 * 计算两个DFA的交集
	 * @return 交集的DFA
	 */
	protected DFA intersect() {
		this.init();
		// 产生两个DFA状态集的笛卡尔积，作为交集的状态集
		this.generateStatusConnection();
		// 产生交集状态集的转换函数，并做初步剪枝
		this.generateTransition();
		
		DFA dfa = new DFA(this.startState, this.statesMap, this.endStateIds);
		// 最小化交集DFA
		dfa.minimize();
		this.cleanup();
		return dfa;
	}

	private void generateStatusConnection(){
		Iterator<DFAState> it1 = this.fa1.getStateIterator();
		while(it1.hasNext()){
			StateID stateId1 = it1.next().getStateId();
			if(stateId1 == DFAState.FAILED_STATE_ID)
				continue;
			Iterator<DFAState> it2 = this.fa2.getStateIterator();
			while(it2.hasNext()){
				StateID stateId2 = it2.next().getStateId();
				if(stateId2 == DFAState.FAILED_STATE_ID)
					continue;
				
				ConnectionState connection = new ConnectionState(stateId1, stateId2);
				
				this.connectionMatrix.put(new Pair<StateID, StateID>(stateId1, stateId2), connection.getStateId());
				this.connectionMap.put(connection.getStateId(), connection);
				
				if(fa1.isStartState(stateId1) && fa2.isStartState(stateId2)){
					this.startState = connection.state;
					this.statesMap.put(this.startState.getStateId(), this.startState);
				}
			}
		}
	}
	
	private void generateTransition() {
		Iterator<StateID> connMapIt = this.connectionMap.keySet().iterator();
		while(connMapIt.hasNext()){
			StateID statusId = connMapIt.next();
			ConnectionState connection = this.connectionMap.get(statusId);
			DFAState status1 = this.fa1.getState(connection.location.getFirst());
			DFAState status2 = this.fa2.getState(connection.location.getSecond());
			
			Iterator<Entry<CharRange, StateID>> charRangeIt1 = status1.getTransitionIterator();
			while(charRangeIt1.hasNext()){
				Entry<CharRange, StateID> entry1 = charRangeIt1.next();
				StateID transId1 = entry1.getValue();
				
				Iterator<Entry<CharRange, StateID>> charRangeIt2 = status2.getTransitionIterator();
				while(charRangeIt2.hasNext()){
					Entry<CharRange, StateID> entry2 = charRangeIt2.next();
					StateID transId2 = entry2.getValue();
					
					List<CharRange> inters = entry1.getKey().intersectWith(entry2.getKey());
					for(int i = 0; i < inters.size(); ++i){
						DFAState transition = this.getConnectionState(transId1, transId2);
						connection.addTransition(inters.get(i), transition.getStateId());
												
						this.statesMap.put(transition.getStateId(), transition);
						if(this.fa1.isEndState(transId1) && this.fa2.isEndState(transId2))
							this.endStateIds.add(transition.getStateId());
					}
				}
			}
		}
	}
	
	private DFAState getConnectionState(Pair<StateID, StateID> loc){
		StateID id = this.connectionMatrix.get(loc);
		if(id == null)
			return DFAState.FAILED_STATE;
		ConnectionState state = this.connectionMap.get(id);
		return state == null ? DFAState.FAILED_STATE : state.state;
	}
	
	private final DFAState getConnectionState(StateID x, StateID y){
		return this.getConnectionState(new Pair<StateID, StateID>(x, y));
	}

}
