package hust.idc.automaton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

final class RegexParser {
	private static final Map<String, String> posixMap;
	
	static {
		posixMap = setPosixMap();
	}
	
	protected static RegularExpression getInstance(Pattern pattern) throws IllegalArgumentException{
		RegularExpression regex = new RegularExpression();
		String patternStr = preProcess(pattern.pattern());
		//System.out.println(patternStr);
		int index = 0;
		while(index < patternStr.length())
		{
			char ch = patternStr.charAt(index);
			//�����.������
			if(ch == '.') {
				regex.addOperand(CharRange.NOT_LINE_BREAK_RANGE);
				++index;
			} else if(ch == '[') {
				CharSequence subSeq = getSequenceInSquareBracket(index, patternStr);
				index += subSeq.length();
				index += 2;
				List<CharRange> list = processSquareBracket(subSeq);
				if(list.isEmpty()){
					regex.clear();
					return regex;
				}
				boolean needBracket = list.size() > 1;
				if(needBracket)
					regex.addOperator('(');
				regex.addOperand(list.get(0));
				for(int j = 1; j < list.size(); ++j){
					regex.addElement(RegexpElementFactory.getOrOperator());
					regex.addOperand(list.get(j));
				}
				if(needBracket)
					regex.addOperator(')');
			} else if(RegexpElement.operatorSupported(ch)) {
				//������(|,*,(,))
				regex.addOperator(ch);
				++index;
			} else if(ch == '{' || ch == '}' || ch == ']') {
				throw new IllegalArgumentException("Unexpected Character at index " + index + ": " + ch);
			} else{
				char[] dest = new char[1];
				index = readChar(index, patternStr, dest);
				regex.addOperand(dest[0]);
			}
		}
		
		return regex;
	}

	private static CharSequence getSequenceInSquareBracket(int index, final CharSequence sequence)
		throws IllegalArgumentException {
		if(index < 0 || index >= sequence.length())
			return null;
		if(sequence.charAt(index) != '[')
			return null;
		
		int bracketCount = 0;
		int i = index + 1;
		boolean flag = false, esc = false;
		for(; i < sequence.length() && !flag; ++i){
			switch(sequence.charAt(i)){
			case '[': 
				if(!esc)
					++bracketCount;
				esc = false;
				break;
			case ']': 
				if(!esc){
					flag = bracketCount == 0;
					--bracketCount;
				}
				esc = false;
				break;
			case CharacterEscaper.ESCAPE_CHAR: esc = !esc; break;
			default: 
				esc = false; 
				break;
			}
		}
		if(!flag)
			throw new IllegalArgumentException("']' not found after '[' at index " + index);
		return sequence.subSequence(index + 1, i - 1);
	}
	
	private static CharSequence getSequenceInBrace(int index, final CharSequence sequence){
		if(index < 0 || index >= sequence.length())
			return null;
		if(sequence.charAt(index) != '{')
			return null;
		
		int i = index + 1;
		boolean flag = false;
		for(; i < sequence.length() && !flag; ++i){
			if(sequence.charAt(i) == '}')
				flag = true;
		}
		if(!flag)
			throw new IllegalArgumentException("'}' not found after '{' at index " + index);
		return sequence.subSequence(index + 1, i - 1);
	}
	
	private static List<CharRange> processSquareBracket(final CharSequence sequence) 
		throws IllegalArgumentException{
		if(sequence == null || sequence.length() <= 0)
			throw new IllegalArgumentException("Illegal Argument");
		
		List<CharSequence> splits = splitSquareBracketSequence(sequence);
		if(splits.size() <= 0)
			throw new IllegalArgumentException("Illegal Argument");
		List<CharRange> list = processSquareBracketWithoutSplit(splits.get(0));
		
		for(int i = 1; i < splits.size(); ++i){
			if(list.isEmpty())
				break;
			List<CharRange> temp = processSquareBracketWithoutSplit(splits.get(i));
			list = CharRangeOperation.intersect(list, temp);
		}
		
		return list;
	}
	
	private static List<CharRange> processSquareBracketWithoutSplit(final CharSequence sequence){
		if(sequence == null || sequence.length() <= 0)
			throw new IllegalArgumentException("Illegal Argument");
		int index = 0;
		while(index < sequence.length() && 
				sequence.charAt(0) == '^'){
			++index;
		}
		boolean complement = (1 == index % 2);
		List<CharRange> list = new ArrayList<CharRange>();
		char[] dest = new char[1];
		while(index < sequence.length()){
			if(sequence.charAt(index) == '['){
				CharSequence subSeq = getSequenceInSquareBracket(index, sequence);
				index += subSeq.length();
				index += 2;
				list.addAll(processSquareBracket(subSeq));
				continue;
			} else if(sequence.charAt(index) == ']'){
				throw new IllegalArgumentException("Illegal Argument");
			} else if(sequence.charAt(index) == RegexpElement.OR_REGULAR_CHAR){
				++index;
				continue;
			} else {
				index = readChar(index, sequence, dest);
				char start = dest[0];
			
				if(index < sequence.length() && sequence.charAt(index) == '-'){
					++index;
					index = readChar(index, sequence, dest);
					if(start <= dest[0])
						list.add(new CharRange(start, dest[0]));
					else
						list.add(new CharRange(dest[0], start));
				} else {
					list.add(new CharRange(start));
				}
			}
		}
		
		return complement ? CharRangeOperation.getComplement(list) : list;
	}
	
	private static List<CharSequence> splitSquareBracketSequence(final CharSequence sequence){
		List<CharSequence> splits = new ArrayList<CharSequence>();
		int bracketCount = 0;
		int start = 0;
		boolean esc = false;
		for(int i = 0; i < sequence.length(); ++i){
			switch(sequence.charAt(i)){
			case '[':
				if(!esc)
					++bracketCount; 
				esc = false;
				break;
			case ']':
				if(!esc)
					--bracketCount; 
				esc = false;
				break;
			case '&':
				if(!esc && bracketCount == 0 && i <= sequence.length()){
					switch(sequence.charAt(++i)){
					case '&':
						splits.add(sequence.subSequence(start, i - 1));
						start = i + 1;
						esc = false;
						break;
					case CharacterEscaper.ESCAPE_CHAR: 
						esc = true;
						break;
					default: 
						esc = false;
						break;
					}
				}
				break;
			case CharacterEscaper.ESCAPE_CHAR:
				esc = !esc;
				break;
			default: break;
			}
		}
		splits.add(sequence.subSequence(start, sequence.length()));
		return splits;
	}
	
	private static int readChar(int start, CharSequence sequence, char[] dest)
		throws IllegalArgumentException {
		if(sequence == null || start < 0 || start >= sequence.length() || 
				dest == null || dest.length <= 0)
			throw new IllegalArgumentException("Illegal Argument");
		
		int index = start;
		char character = sequence.charAt(index++);
		if(character == CharacterEscaper.ESCAPE_CHAR){
			// escape char
			if(index >= sequence.length())
				throw new IllegalArgumentException("Illegal Argument");
			
			char ch = sequence.charAt(index++);
			switch(ch){
			case 'u': 
				if(index + 4 > sequence.length())
					throw new IllegalArgumentException("Illegal argument after \\u");
				CharSequence chstru = sequence.subSequence(index, index + 4);
				try{
					dest[0] = (char) Integer.valueOf(chstru.toString(), 16).intValue();
					return index + 4;
				} catch(Exception e){
					throw new IllegalArgumentException("Illegal argument after \\u");
				}
			case 'x': 
				if(index + 2 > sequence.length())
					throw new IllegalArgumentException("Illegal argument after \\x");
				CharSequence chstrx = sequence.subSequence(index, index + 2);
				try{
					dest[0] = (char) Integer.valueOf(chstrx.toString(), 16).intValue();
					return index + 2;
				} catch(Exception e){
					throw new IllegalArgumentException("Illegal argument after \\u");
				}
			case '0':
				CharSequence chstr0 = sequence.subSequence(index, Math.min(index + 3, sequence.length()));
				if(chstr0.length() <= 0)
					throw new IllegalArgumentException("Illegal Argument after \\0");
				char[] chs = new char[]{'\0', '\0', '\0'};
				int offset = 0;
				for(; offset < chs.length && offset < chstr0.length(); ++offset){
					if(chstr0.charAt(offset) >= '0' && chstr0.charAt(offset) <= '7')
						chs[offset] = chstr0.charAt(offset);
					else
						break;
				}
				if(offset == 3 && (chs[0] < '0' || chs[0] > '3'))
					--offset;
				String str = new String(chs, 0, offset);
				str.replaceAll("\0", "");
				if(str.length() <= 0)
					throw new IllegalArgumentException("Illegal Argument after \\0");
				dest[0] = (char) Integer.valueOf(str, 8).intValue();
				return index + str.length();
			case 'e': dest[0] = (char) 0x001b; break;
			case 'n': dest[0] = '\n'; break;
			case 'r': dest[0] = '\r'; break;
			case 't': dest[0] = '\t'; break;
			case 'f': dest[0] = '\f'; break;
			case 'v': dest[0] = (char) 0x000b; break;
			case '*':
			case '+':
			case '?':
			case '.':
			case '^':
			case '$':
			case '&':
			case '(':
			case ')':
			case '[':
			case ']':
			case '{':
			case '}':
			case '|':
			case '-':
			case CharacterEscaper.ESCAPE_CHAR: dest[0] = ch; break;
			default: 
				throw new IllegalArgumentException("Illegal Argument after escape '\\'");
			}
			return index;
		} else {
			dest[0] = character;
			return index;
		}
	}
	
	private static String preProcess(final String regex) throws IllegalArgumentException{
		StringBuilder sb = new StringBuilder(regex);
		
		int i = 0;
		while(i < sb.length()){
			switch(sb.charAt(i)){
			case '+': 
				i = processPlusOperator(sb, i);
				break;
			case '{':
				i = processBraceOperator(sb, i);
				break;
			case '}':
				throw new IllegalArgumentException("Unexpected '}' at index " + i);
			case CharacterEscaper.ESCAPE_CHAR: 
				i = processEscape(sb, i);
				break;
			default: ++i; break;
			}
		}
		//处理+
		//processPlusOperator(sb);
		//处理{}
//		processBraceOperator(sb);
		//处理\d，即数字，等价于[0-9]；夐理\D，即非数字，等价于[^0-9]
//		processBackslashd(sb);
		return sb.toString();
	}
	
	private static CharSequence readUnitBack(StringBuilder sequence, int index){
		if(sequence == null || sequence.length() <= 0)
			throw new IllegalArgumentException("Empty Sequence");
		if(index < 0 || index >= sequence.length())
			throw new IllegalArgumentException("Out of Bound");
		
		switch(sequence.charAt(index)){
		case ')': 
			if(index < 2)
				throw new IllegalArgumentException("error format: ')' could not at the beginning of a pattern");
			int roundEscCount = getEscCountBack(sequence, index - 1);
			if(roundEscCount % 2 != 0){
				return sequence.subSequence(index - 1, index + 1);
			}
				
			int i1, roundBracket = 0;
			boolean roundFlag = false;
			for (i1 = index - roundEscCount - 1; i1 >= 0 && !roundFlag; --i1) {
				switch(sequence.charAt(i1)){
				case ')':
					roundEscCount = getEscCountBack(sequence, i1 - 1);
					if(roundEscCount % 2 == 0){
						++roundBracket; 
					}
					i1 -= roundEscCount;
					break;
				case '(': 
					roundEscCount = getEscCountBack(sequence, i1 - 1);
					if(roundEscCount % 2 == 0){
						roundFlag = roundBracket == 0; 
						--roundBracket;
					}
					i1 -= roundEscCount;
					break;
				default : break;
				}
			}
			if(!roundFlag)
				throw new IllegalArgumentException("'(' not found before ')' at index " + index);
			return sequence.subSequence(i1 + 1, index + 1);
		case ']':
			if(index < 2)
				throw new IllegalArgumentException("error format: ']' could not at the beginning of a pattern");
			int squareEscCount = getEscCountBack(sequence, index - 1);
			if(squareEscCount % 2 != 0){
				return sequence.subSequence(index - 1, index + 1);
			}
			//flag用于嵌�M的（�?
			int i2, squareBracket = 0;
			boolean squareFlag = false;
			for (i2 = index - squareEscCount - 1; i2 >= 0 && !squareFlag; --i2) {
				switch(sequence.charAt(i2)){
				case ']': 
					squareEscCount = getEscCountBack(sequence, index - 1);
					if(squareEscCount % 2 == 0)
						++squareBracket; 
					i2 -= squareEscCount;
					break;
				case '[': 
					squareEscCount = getEscCountBack(sequence, index - 1);
					if(squareEscCount % 2 == 0){
						squareFlag = squareBracket == 0; 
						--squareBracket; 
					}
					i2 -= squareEscCount;
					break;
				default : break;
				}
			}
			if(!squareFlag)
				throw new IllegalArgumentException("'[' not found before ']' at index " + index);
			return sequence.subSequence(i2 + 1, index + 1);
		default:
			int unitStart = index ;
			int escIndex = sequence.lastIndexOf("\\", index);
			if(getEscCountBack(sequence, escIndex - 1) % 2 != 0){}
			else if(escIndex == -1 || escIndex < index - 6){}
			else {
				if(escIndex == index - 1)
					unitStart = escIndex;
				else if(escIndex == index - 5 && sequence.charAt(escIndex + 1) == 'u')
					unitStart = escIndex;
				else if(escIndex == index - 2 && sequence.charAt(escIndex + 1) == 'x')
					unitStart = escIndex;
				else if(escIndex >= index - 3 && sequence.charAt(escIndex + 1) == '0')
					unitStart = escIndex;
				else
					throw new IllegalArgumentException("Unknow resouce");
			}
			return sequence.subSequence(unitStart, index + 1);
		}
	}
	
	private static int getEscCountBack(CharSequence sequence, int index){
		if(sequence == null || sequence.length() <= 0)
			return 0;
		if(index < 0 || index >= sequence.length())
			return 0;
		
		int escCount = 0;
		for(int i = index; i >= 0; --i){
			if(sequence.charAt(i) != CharacterEscaper.ESCAPE_CHAR)
				return escCount;
			++escCount;
		}
		
		return escCount;
	}
	
	//处理表达式中�?操作�?
	private static int processPlusOperator(StringBuilder sb, int index) 
		throws IllegalArgumentException{
		if(sb.charAt(index) != '+')
			return index;
		if(index == 0)
			throw new IllegalArgumentException("error format: '+' could not at the beginning of a pattern");
		
		CharSequence unit = readUnitBack(sb, index - 1);
		sb.replace(index, index + 1, unit.toString() + "*");
		return index + unit.length() + 1;
	}

	//处理表达式中的{}操作�?
	/*
	 * 目前仅支持单�?双位整数，例如{1,9}，{11,22}，不支持{100,200}
	 */
	private static int processBraceOperator(StringBuilder sb, int index)
		throws IllegalArgumentException{
		if(sb.charAt(index) != '{')
			return index;
		if(index == 0)
			throw new IllegalArgumentException("error format: '{' could not at the beginning of a pattern");
		
		CharSequence braceSeq = getSequenceInBrace(index, sb);
		List<CharSequence> splits = new ArrayList<CharSequence>();
		int start = 0;
		boolean canSplit = false;
		for(int i = 0; i < braceSeq.length(); ++i){
			if(braceSeq.charAt(i) == ','){
				canSplit = true;
				splits.add(braceSeq.subSequence(start, i));
				start = i + 1;
			}
		}
		if(start < braceSeq.length())
			splits.add(braceSeq.subSequence(start, braceSeq.length()));
		
		if(splits.isEmpty() || splits.size() > 2)
			throw new IllegalArgumentException("error format in '{ }'");
		
		try{
			// get repeat times
			int min = Integer.parseInt(splits.get(0).toString());
			int max = min;
			if(splits.size() == 2)
				max = Integer.parseInt(splits.get(1).toString());
			else if(canSplit)
				max = Integer.MAX_VALUE;
			
			// get repeat unit
			CharSequence unit = readUnitBack(sb, index - 1);
			
			StringBuilder replace = new StringBuilder();
			int i = 0;
			for(; i < min; ++i)
				replace.append(unit);
			
			if(max < Integer.MAX_VALUE){
				for(; i < max; ++i)
					replace.append(unit + "?");
			} else
				replace.append(unit + "*");
			
			sb.replace(index - unit.length(), index + braceSeq.length() + 2, replace.toString());
			return index - unit.length() + replace.length();
		} catch(Exception e){
			throw new IllegalArgumentException(e.getMessage() + "\n\terror format in '{ }'");
		}
	}
	
	//处理\d，即数字，等价于[0-9]；夐理\D，即非数字，等价于[^0-9]
	private static int processEscape(StringBuilder sb, int index) throws IllegalArgumentException{
		if(sb.charAt(index) != CharacterEscaper.ESCAPE_CHAR)
			return index;
		String replace = new String();
		int last = index + 2;
		switch(sb.charAt(index + 1)){
		case 'd': replace = "[" + CharRange.DIGIT_RANGE.toString() + "]"; break;
		case 'D': replace = "[" + CharRange.NOT_DIGIT_RANGE.toString() + "]"; break;
		case 'w': replace = "[" + CharRange.LOWER_CASE_RANGE.toString() + 
								CharRange.UPPER_CASE_RANGE.toString() + "_" + 
								CharRange.DIGIT_RANGE.toString() + "]"; break;
		case 'W': replace = "[^" + CharRange.LOWER_CASE_RANGE.toString() + 
								CharRange.UPPER_CASE_RANGE.toString() + "_" + 
								CharRange.DIGIT_RANGE.toString() + "]"; break;
		case 's': replace = "[ \\t-\\r]"; break;
		case 'S': replace = "[^ \\t-\\r]"; break;
		case 'p':
			if(index + 2 >= sb.length() || sb.charAt(index + 2) != '{')
				throw new IllegalArgumentException("\\p is not a illegal regex");
			boolean error = true;
			for(last = index + 3; last < sb.length(); ++last){
				if(sb.charAt(last) == '}'){
					error = false;
					break;
				}
			}
			if(error)
				throw new IllegalArgumentException("\"}\" is not found after index: " + (index + 2));
			replace = "[" + getPosixRegex(sb.substring(index + 3, last)) + "]";
			++last;
			break;
		default: replace = null; break;
		}
		if(replace != null){
			sb.replace(index, last, replace);
			return index + replace.length();
		} else {
			return index + 2;
		}
	}
	
	//处理POSIX 字符�?
	/*
	 * 未完待续
	 */
	private static String getPosixRegex(String posix) throws IllegalArgumentException{
		String regex = posixMap.get(posix);
		if(regex == null)
			throw new IllegalArgumentException("Unsupported Posix Regex");
		return regex;
	}
	
	private final static Map<String, String> setPosixMap() {
		// TODO Auto-generated method stub
		Map<String, String> map = new HashMap<String, String>();
		
		map.put("Lower", CharRange.LOWER_CASE_RANGE.toString()); 
		map.put("Upper", CharRange.UPPER_CASE_RANGE.toString());
		map.put("ASCII", CharRange.ASCII_RANGE.toString());
		map.put("Alpha", map.get("Lower") + map.get("Upper"));
		map.put("Digit", CharRange.DIGIT_RANGE.toString());
		map.put("XDigit", map.get("Digit") + "a-fA-F");
		map.put("Alnum", map.get("Digit") + map.get("Alpha"));
		map.put("Punct", "!-/:-@\\[-`\\{-~");
		map.put("Graph", CharRange.GRAPH_RANGE.toString());
		map.put("Print", map.get("Graph") + " ");
		map.put("Blank", " \\t");
		map.put("Space", " \\t-\\r");
		map.put("Cntrl", "\\x00-\\x1F\\x7F");

		map.put("javaLowerCase", map.get("Lower"));
		map.put("javaUpperCase", map.get("Upper"));
		map.put("javaWhitespace", null);
		map.put("javaMirrored", null);

		// Add Unicode Segment at here
		map.put("InGreek", "\\u0370-\\u03FF");
		map.put("InCJKUnifiedIdeographsExtensionA", "\\u3400-\\u4DBF");
		map.put("InCJKUnifiedIdeographs", "\\u4E00-\\u9FAF");
		map.put("Sc", "\\u20A0-\\u20CF");
		map.put("Lu", map.get("Upper"));
		map.put("Ll", map.get("Lower"));
		map.put("L", map.get("Alpha"));
		map.put("all", CharRange.UNIVERSAL_RANGE.toString());
		
		return map;
	}

}
