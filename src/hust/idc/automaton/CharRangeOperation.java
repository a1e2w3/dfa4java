package hust.idc.automaton;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * Implementation of operations of character set which is presented by a range
 * 
 * @author WangCong
 * 
 */
final class CharRangeOperation {

	/**
	 * Get the complement of the union of a set of character set
	 * 
	 * @param rangeList
	 *            A set of character set
	 * @return The complement presented by a set of character set
	 * @throws Throwable
	 */
	protected static List<CharRange> getComplement(
			final Collection<? extends CharRange> rangeList) {
		List<CharRange> complement = new ArrayList<CharRange>();
		complement.add(CharRange.UNIVERSAL_RANGE);

		Iterator<? extends CharRange> rangeListIt = rangeList.iterator();
		while (rangeListIt.hasNext()) {
			CharRange cr = rangeListIt.next();
			List<CharRange> intersections = new ArrayList<CharRange>();
			for (int j = 0; j < complement.size(); ++j) {
				CharRange crComp = cr.complement();
				if(crComp == null){
					complement.clear();
					return complement;
				}
				intersections.addAll(intersect(crComp, complement
						.get(j)));
			}
			complement.clear();
			complement = intersections;
		}

		return complement;
	}

	/**
	 * 计算两个字符区间的交�?
	 * 
	 * @param range1
	 *            字符区间1
	 * @param range2
	 *            字符区间2
	 * @return 两个字符区间的交集，用一列字符区间的并集的形式表�?
	 */
	protected static List<CharRange> intersect(final CharRange range1,
			final CharRange range2) {
		List<CharRange> intersections = new ArrayList<CharRange>();
		if (range1 == null || range2 == null)
			return intersections;
		if (range1.isInRange() && range2.isInRange()) {
			char start = CharRange.CHAR_MIN_VALUE, end = CharRange.CHAR_MAX_VALUE;
			if (range2.charIn(range1.getStart())) {
				start = range1.getStart();
			} else if (range1.charIn(range2.getStart())) {
				start = range2.getStart();
			} else {
				return intersections;
			}

			if (range2.charIn(range1.getEnd())) {
				end = range1.getEnd();
			} else if (range1.charIn(range2.getEnd())) {
				end = range2.getEnd();
			} else {
				return intersections;
			}

			intersections.add(new CharRange(start, end));
			return intersections;
		} else if (!range1.isInRange() && !range2.isInRange()) {
			if (range1.getStart() > CharRange.CHAR_MIN_VALUE) {
				CharRange range11 = new CharRange(CharRange.CHAR_MIN_VALUE,
						(char) (range1.getStart() - 1), true);
				intersections.addAll(intersect(range2, range11));
			}
			if (range1.getEnd() < CharRange.CHAR_MAX_VALUE) {
				CharRange range12 = new CharRange((char) (range1.getEnd() + 1),
						CharRange.CHAR_MAX_VALUE, true);
				intersections.addAll(intersect(range2, range12));
			}
			return intersections;
		} else {
			if (!range2.isInRange()) {
				return intersect(range1, range2);
			}

			if (range1.getStart() > CharRange.CHAR_MIN_VALUE) {
				CharRange range11 = new CharRange(CharRange.CHAR_MIN_VALUE,
						(char) (range1.getStart() - 1), true);
				intersections.addAll(intersect(range2, range11));
			}
			if (range1.getEnd() < CharRange.CHAR_MAX_VALUE) {
				CharRange range12 = new CharRange((char) (range1.getEnd() + 1),
						CharRange.CHAR_MAX_VALUE, true);
				intersections.addAll(intersect(range2, range12));
			}
			return intersections;
		}
	}

	protected static List<CharRange> intersect(CharRange range,
			Collection<? extends CharRange> rangeList) {
		List<CharRange> intersection = new ArrayList<CharRange>();

		if (range == null || rangeList == null)
			return intersection;

		for (CharRange cr : rangeList) {
			intersection.addAll(intersect(range, cr));
		}

		return intersection;
	}

	protected static List<CharRange> intersect(
			Collection<? extends CharRange> rangeList1,
			Collection<? extends CharRange> rangeList2) {
		List<CharRange> intersection = new ArrayList<CharRange>();

		for (CharRange cr : rangeList1) {
			intersection.addAll(intersect(cr, rangeList2));
		}

		return intersection;
	}

	protected static List<CharRange> minus(CharRange range, CharRange subtractor) {
		return intersect(range, subtractor.complement());
	}

}
